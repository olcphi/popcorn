PopCorn
==========
An deploy tool for developers to run distributed programs on Hadoop Yarn. It uses configuration file to
indicate how to set up containers' running environment.

Dependency
----------
* Maven

Build
----------
After installing maven, enter command "mvn package" in the directory which contains pom.xml. Once build succeeded,
the jar file will be generated in target directory.

Generated Environment Variables
----------
For each container, these environment variables will be set automatically by application master.

* **RANK:**
the rank of the container is a unique number starts from 0 among containers of the same type.
* **HOST:**
the hostname the container will run.
* **IP:**
the IP address the container will run.
* **NUM_PROCESSES:**
the number of processes the container should run.

Generated Files
----------
For each container, these files will be generated and put in **executeDir** specified by **containerConfs**
automatically.

* **machine_list:**
for container type without recovery and flexibility, it will generate a machine list.
Its machine list filename is the type of container concatenated with "_machine_list". For example,
for container type "slave", its machine list filename is "slave_machine_list". For each
container, all generated machine list files will be put in its **executeDir** specified
by **containerConfs**.

Configuration Attributes
----------
Attributes of configuration file are in [JSON format](https://en.wikipedia.org/wiki/JSON).

* **numPorts:**
the number of required ports. These ports will be assigned ports by application master,
ports will be saved in environment variables PORT_ARG_0, PORT_ARG_1, ..., PORT_ARG_(numPorts - 1)
for each container.
* **arguments:**
used to quickly add commandline options, it's made up of a map.
The key is an attribute path, the value is an additional commandline options.
For example, when key is "numPorts", value is "np", it means that when people
enter -np 1 in the command line, numPorts will be set to 1 in the configuration.
* **jarPath:**
the path of jar file of application master, can be either local or hdfs file.
* **containerConfs:**
containerConfs is a map. The key is a string used to specify container type, such as
"worker", "server", "slave" and so on. The value is a also a map and has the following attributes:
    * **numProcesses:**
    the expected number of processes this container type should run. Must be filled.
    * **processMemory:**
    the memory a process can use. Must be filled.
    * **processVCores:**
    the virtual cores a process can use. Must be filled.
    * **multipleProcessesPerContainer:**
    whether one container can run multiple processes. Default is "false".
    * **waitForCompletion:**
    whether to wait for all containers in the type to complete. Default is "false".
    * **observeOutput:**
    whether to observe stdout and stderr of all containers in this type. Default is "false".
    * **recoverable:**
    when failure of containers occur, whether to restart the containers. Default is "false".
    * **flexible:**
    whether this kind of containers can work when there is at least one container,
    when the container type are flexible, it cannot be waited for completion. Default is "false".
    * **executeDir:**
    the directory where the terminal start to run program. Default is "".
    * **executeFilename:**
    then filename which should be started by the terminal, must be filled.
    * **machineListFormat:**
    the format of each line in machine list. In the format string, all reserved words
    in windows environment variables style (e.g. %IP%, %HOST%) will be replaced by
    variables set in **envVars** or generated by AM.
    * **resourcePaths:**
    an array containing resource paths, can be either local or hdfs file paths. Default is an
    empty array.
    * **envVars:**
    a key-value map used to specified the environment variable we want to put in the container.
    Default is an empty map
* **launchOrder:**
An array used to specify the order of launching containers,
container type should exist in containerConfs. Default is an empty array.
* **exclusions:**
An two dimension array used to specify the exclusion relation between containers.
The inner array contains two elements, which means that these two container type
cannot be allocated in the same node. Default is an empty array.

Commandline Options
----------
    -help                   Print commandline options.
    -queue <arg>            Yarn queue to submit job to. Default to "default".
    -sync                   Enable synchronous mode, in which client will wait
                            for app to finish, and stream AM stdout in real
                            time.
    -sync_port <arg>        Port to be used in client for sync, when -s is enabled.
                            If sync port is not set, it will be assigned a port automatically by AM.
    -receive_port <arg>     Port to be used in am for recive containers' output.
                            If receiver port is not set, it will be assigned a port automatically by AM.
    -config <arg>           The file path of configuration file, must be local file.
    -alloctime <arg>        Maximum allocation time. If we cannot get enough.
                            container in this time, AM wil exit. Default 120s.
    -exectime <arg>         Maximum execution time. If mpiexec does not exit
                            in this time, AM will exit. Default 24h.
    -priority <arg>         The priority of application. Default is 1.
    -appname <arg>          Application name to appear in Yarn. Default is "PopCorn Application".
    -am_memory <arg>        Memory of application master. Default is 1024MB.
    -am_vcores <arg>        Virtual cores of application master. Default is 4.
