package org.apache.hadoop.yarn.applications.util;

import org.apache.hadoop.yarn.applications.constants.AMConstants;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by v-yihhe on 1/19/2016.
 */
public class TestMachineListFormatter {

  private HashMap<String, String> genEnvToVar = new HashMap<String, String>() {{
    put(AMConstants.GEN_ENV_HOST, "localhost");
    put(AMConstants.GEN_ENV_RANK, "0");
    put(AMConstants.GEN_ENV_NUM_PROCESSES, "1");
    put(AMConstants.GEN_ENV_IP, "127.0.0.1");
    put(AMConstants.GEN_ENV_PORT_ARG + "_0", "10000");
  }};

  @Test
  public void hostWithPortArgsTest() {
    String machineListFormat =
        "%" + AMConstants.GEN_ENV_HOST + "% %" + AMConstants.GEN_ENV_NUM_PROCESSES + "%";
    Assert.assertEquals("localhost 1", MachineListFormatter.format(
        machineListFormat,
        genEnvToVar,
        new HashMap<String, String>()));
  }

  @Test
  public void rankWithIpWithPortArg_0Test() {
    String machineListFormat =
        "%" + AMConstants.GEN_ENV_RANK + "%:%" + AMConstants.GEN_ENV_IP + "%:%" + AMConstants.GEN_ENV_PORT_ARG + "_0%";
    Assert.assertEquals("0:127.0.0.1:10000", MachineListFormatter.format(
        machineListFormat,
        genEnvToVar,
        new HashMap<String, String>()));
  }

  @Test
  public void ipWithPortArg_0WithThreadCount() {
    String machineListFormat =
        "%" + AMConstants.GEN_ENV_IP + "%:%" + AMConstants.GEN_ENV_PORT_ARG + "_0%:%THREAD_COUNT%";
    Map<String, String> envVars = new HashMap<String, String>();
    envVars.put("THREAD_COUNT", "5");
    Assert.assertEquals("127.0.0.1:10000:5", MachineListFormatter.format(
        machineListFormat,
        genEnvToVar,
        envVars));
  }
}
