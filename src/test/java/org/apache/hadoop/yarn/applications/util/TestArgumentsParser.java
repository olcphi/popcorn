package org.apache.hadoop.yarn.applications.util;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by v-yihhe on 1/7/2016.
 */
public class TestArgumentsParser {
  @Test
  public void ignoreUnrecognizedOption() {
    Options opts = new Options();
    opts.addOption("a", true, "");
    String[] args = new String[] {"-n", "1", "-a", "2"};
    CommandLine cliParser;
    try {
      cliParser = new ArgumentsParser().parse(opts, args);
    } catch (Exception exp) {
      Assert.fail();
      return;
    }
    Assert.assertEquals("2", cliParser.getOptionValue("a"));
  }
}
