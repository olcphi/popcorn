package org.apache.hadoop.yarn.applications.am;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.yarn.api.records.Resource;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

/**
 * Created by v-yihhe on 12/21/2015.
 */
public class TestContainerPlacementStrategy {

  private static Configuration conf;

  private String[] hosts;

  private Resource[] used;

  private Resource[] capability;

  @BeforeClass
  public static void beforeClass() {
    conf = new Configuration();
    conf.set("yarn.scheduler.minimum-allocation-mb", "1024");
    conf.set("yarn.scheduler.maximum-allocation-mb", "8192");
    conf.set("yarn.scheduler.minimum-allocation-vcores", "1");
    conf.set("yarn.scheduler.maximum-allocation-vcores", "8");
  }

  @Test
  public void succeedToGeneratePlacements() {
    // setup nodeReports
    hosts = new String[] {"host1", "host2"};
    used = new Resource[hosts.length];
    capability = new Resource[hosts.length];
    for (int idx = 0; idx != hosts.length; ++idx) {
      used[idx] = Resource.newInstance(1024, 2);
      capability[idx] = Resource.newInstance(3072, 4);
    }

    List<List<String>> exclusions = new ArrayList<List<String>>();
    exclusions.add(Arrays.asList("server", "server"));
    exclusions.add(Arrays.asList("worker", "worker"));

    YarnClient mockYarnClient = new MockYarnClientImpl(hosts, used, capability);

    // setup containerPlacementStrategy
    ContainerPlacementStrategy containerPlacementStrategy =
        new ContainerPlacementStrategy(conf, mockYarnClient, exclusions);

    // setup container placement requests
    Map<String, ContainerPlacementStrategy.ContainerPlacementRequest> requests =
        new HashMap<String, ContainerPlacementStrategy.ContainerPlacementRequest>();
    int numRequiredProcesses = 2;
    Resource needResource = Resource.newInstance(1024, 1);
    ContainerPlacementStrategy.ContainerPlacementRequest containerRequest =
        new ContainerPlacementStrategy.ContainerPlacementRequest(numRequiredProcesses, false, needResource);
    String[] containerNames = new String[] {"server", "worker"};
    for (String containerName : containerNames) {
      requests.put(containerName, containerRequest);
    }
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToNewPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToCurrentPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();

    // invokde generatePlacements method and get new placements
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> newPlacements =
        containerPlacementStrategy.generatePlacements(typeToNewPlacementsMap, typeToCurrentPlacementsMap, requests);

    // check result
    for (String containerName : containerNames) {
      Set<ContainerPlacementStrategy.ContainerPlacement> set = newPlacements.get(containerName);
      for (ContainerPlacementStrategy.ContainerPlacement containerPlacement : set) {
        Assert.assertEquals(Integer.valueOf(1), containerPlacement.getNumProcesses());
      }
    }
  }

  @Test
  public void succeedToGeneratePlacementsWithMultipleProcessesPerContainer() {
    // setup yarnClient
    hosts = new String[] {"host1", "host2", "host3", "host4"};
    used = new Resource[hosts.length];
    capability = new Resource[hosts.length];
    for (int idx = 0; idx != hosts.length; ++idx) {
      used[idx] = Resource.newInstance(0, 0);
      capability[idx] = Resource.newInstance(16 * 1024, 16 * 1);
    }
    YarnClient mockYarnClient = new MockYarnClientImpl(hosts, used, capability);

    // setup containerPlacementStrategy
    List<List<String>> exclusions = new ArrayList<List<String>>();
    exclusions.add(Arrays.asList("slave", "slave"));
    ContainerPlacementStrategy containerPlacementStrategy =
        new ContainerPlacementStrategy(conf, mockYarnClient, exclusions);

    Map<String, ContainerPlacementStrategy.ContainerPlacementRequest> requests =
        new HashMap<String, ContainerPlacementStrategy.ContainerPlacementRequest>();
    int numRequiredProcesses = 32;
    Resource needResource = Resource.newInstance(512, 1);

    // setup container placement requests
    ContainerPlacementStrategy.ContainerPlacementRequest containerRequest =
        new ContainerPlacementStrategy.ContainerPlacementRequest(numRequiredProcesses, true, needResource);
    String[] containerNames = new String[] {"slave"};
    for (String containerName : containerNames) {
      requests.put(containerName, containerRequest);
    }
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToNewPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToCurrentPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();

    // invokde generatePlacements method and get new placements
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> newPlacements =
        containerPlacementStrategy.generatePlacements(typeToNewPlacementsMap, typeToCurrentPlacementsMap, requests);

    // check the result
    for (String containerName : containerNames) {
      Set<ContainerPlacementStrategy.ContainerPlacement> containerPlacementSet = newPlacements.get(containerName);
      Map<String, Set<Integer>> hostToNumProcessesSetMap = new HashMap<String, Set<Integer>>();
      for (ContainerPlacementStrategy.ContainerPlacement containerPlacement : containerPlacementSet) {
        Set<Integer> numProcessesSet = hostToNumProcessesSetMap.get(containerPlacement.getHost());
        if (numProcessesSet == null) {
          numProcessesSet = new HashSet<Integer>();
          hostToNumProcessesSetMap.put(containerPlacement.getHost(), numProcessesSet);
        }
        numProcessesSet.add(containerPlacement.getNumProcesses());
      }
      for (String host : hosts) {
        Set<Integer> numProcessesSet = hostToNumProcessesSetMap.get(host);
        Assert.assertEquals(1, numProcessesSet.size());
        Assert.assertTrue(numProcessesSet.contains(8));
      }
    }
  }

  @Test
  public void succeedToGeneratePlacementsWithMultipleContainersPerNodeAndMultipleProcessesPerContainer() {
    // setup yarnClient
    hosts = new String[] {"host1"};
    used = new Resource[hosts.length];
    capability = new Resource[hosts.length];
    for (int idx = 0; idx != hosts.length; ++idx) {
      used[idx] = Resource.newInstance(0, 0);
      capability[idx] = Resource.newInstance(16 * 1024, 16 * 1);
    }
    YarnClient mockYarnClient = new MockYarnClientImpl(hosts, used, capability);

    // setup containerPlacementStrategy
    List<List<String>> exclusions = new ArrayList<List<String>>();
    ContainerPlacementStrategy containerPlacementStrategy =
        new ContainerPlacementStrategy(conf, mockYarnClient, exclusions);

    // setup container placement requests
    Map<String, ContainerPlacementStrategy.ContainerPlacementRequest> requests =
        new HashMap<String, ContainerPlacementStrategy.ContainerPlacementRequest>();
    int numRequiredProcesses = 9;
    Resource needResource = Resource.newInstance(512, 1);
    ContainerPlacementStrategy.ContainerPlacementRequest containerRequest =
        new ContainerPlacementStrategy.ContainerPlacementRequest(numRequiredProcesses, true, needResource);
    String[] containerNames = new String[] {"slave"};
    for (String containerName : containerNames) {
      requests.put(containerName, containerRequest);
    }
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToNewPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToCurrentPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();

    // invokde generatePlacements method and get new placements
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> newPlacements =
        containerPlacementStrategy.generatePlacements(typeToNewPlacementsMap, typeToCurrentPlacementsMap, requests);

    // check the result
    for (String containerName : containerNames) {
      Set<ContainerPlacementStrategy.ContainerPlacement> set = newPlacements.get(containerName);
      Set<Integer> numProcessesSet = new HashSet<Integer>();
      for (ContainerPlacementStrategy.ContainerPlacement containerPlacement : set) {
        Assert.assertEquals("host1", containerPlacement.getHost());
        numProcessesSet.add(containerPlacement.getNumProcesses());
      }
      Assert.assertEquals(2, numProcessesSet.size());
      Assert.assertTrue(numProcessesSet.contains(1));
      Assert.assertTrue(numProcessesSet.contains(8));
    }
  }

  @Test
  public void succeedToGeneratePlacementsWithExistedPlacements() {
    // setup nodeReports
    hosts = new String[10];
    for (int idx = 0; idx != 10; ++idx) {
      hosts[idx] = "host" + (idx + 1);
    }
    used = new Resource[hosts.length];
    capability = new Resource[hosts.length];
    for (int idx = 0; idx != hosts.length; ++idx) {
      used[idx] = Resource.newInstance(0, 0);
      capability[idx] = Resource.newInstance(2048, 2);
    }
    List<List<String>> exclusions = new ArrayList<List<String>>();
    for (int i = 0; i != 10; ++i) {
      for (int j = i + 1; j != 10; ++j) {
        List<String> exclusion = Arrays.asList("container" + i, "container" + j);
        exclusions.add(exclusion);
      }
    }
    MockYarnClientImpl mockYarnClientImpl = new MockYarnClientImpl(hosts, used, capability);

    // setup containerPlacementStrategy
    ContainerPlacementStrategy containerPlacementStrategy =
        new ContainerPlacementStrategy(conf, mockYarnClientImpl, exclusions);

    // setup container placement requests
    Map<String, ContainerPlacementStrategy.ContainerPlacementRequest> requests =
        new HashMap<String, ContainerPlacementStrategy.ContainerPlacementRequest>();
    int numRequiredProcesses = 1;
    Resource needResource = Resource.newInstance(1024, 1);
    ContainerPlacementStrategy.ContainerPlacementRequest containerRequest =
        new ContainerPlacementStrategy.ContainerPlacementRequest(numRequiredProcesses, false, needResource);
    String[] containerTypes = new String[10];
    for (int i = 0; i != 10; ++i) {
      containerTypes[i] = "container" + i;
    }
    for (String containerName : containerTypes) {
      requests.put(containerName, containerRequest);
    }
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToNewPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();

    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToCurrentPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();

    int number = 0;
    for (String containerType : containerTypes) {
      ContainerPlacementStrategy.ContainerPlacement containerPlacement =
          new ContainerPlacementStrategy.ContainerPlacement(1, "host" + (++number));
      allocate(
          mockYarnClientImpl,
          typeToCurrentPlacementsMap,
          containerType,
          containerPlacement,
          needResource);
    }

    // invokde generatePlacements method and get new placements
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> newPlacements =
        containerPlacementStrategy.generatePlacements(typeToNewPlacementsMap, typeToCurrentPlacementsMap, requests);

    // check result
    for (String containerType : containerTypes) {
      Set<ContainerPlacementStrategy.ContainerPlacement> newSet = newPlacements.get(containerType);
      Assert.assertEquals(1, newSet.size());
      ContainerPlacementStrategy.ContainerPlacement newContainerPlacement = newSet.iterator().next();
      Set<ContainerPlacementStrategy.ContainerPlacement> originSet = typeToCurrentPlacementsMap.get(containerType);
      Assert.assertEquals(1, newSet.size());
      ContainerPlacementStrategy.ContainerPlacement originContainerPlacement = originSet.iterator().next();
      Assert.assertEquals(newContainerPlacement.getHost(), originContainerPlacement.getHost());
    }
  }

  @Test
  public void failToGeneratePlacements() {
    // setup yarnClient
    hosts = new String[] {"host1", "host2"};
    used = new Resource[hosts.length];
    capability = new Resource[hosts.length];
    for (int idx = 0; idx != hosts.length; ++idx) {
      used[idx] = Resource.newInstance(1024, 2);
      capability[idx] = Resource.newInstance(3072, 4);
    }
    YarnClient mockYarnClient = new MockYarnClientImpl(hosts, used, capability);

    // setup containerPlacementStrategy
    List<List<String>> exclusions = new ArrayList<List<String>>();
    exclusions.add(Arrays.asList("server", "server"));
    exclusions.add(Arrays.asList("worker", "worker"));
    ContainerPlacementStrategy containerPlacementStrategy =
        new ContainerPlacementStrategy(conf, mockYarnClient, exclusions);

    // setup container placement requests
    Map<String, ContainerPlacementStrategy.ContainerPlacementRequest> requests =
        new HashMap<String, ContainerPlacementStrategy.ContainerPlacementRequest>();
    int numRequiredProcesses = 2;
    Resource needResource = Resource.newInstance(1024, 2);

    // invokde generatePlacements method and get new placements
    ContainerPlacementStrategy.ContainerPlacementRequest containerRequest =
        new ContainerPlacementStrategy.ContainerPlacementRequest(numRequiredProcesses, false, needResource);

    // check the result
    String[] containerNames = new String[] {"server", "worker"};
    for (String containerName : containerNames) {
      requests.put(containerName, containerRequest);
    }
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToNewPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToCurrentPlacementsMap =
        new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();

    // invokde generatePlacements method and get new placements
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> newPlacements =
        containerPlacementStrategy.generatePlacements(typeToNewPlacementsMap, typeToCurrentPlacementsMap, requests);
    // check placements
    Assert.assertNull(newPlacements);
  }

  private static void allocate(
      MockYarnClientImpl mockYarnClientImpl,
      Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToPlacementsMap,
      String containerType,
      ContainerPlacementStrategy.ContainerPlacement containerPlacement,
      Resource processResource) {
    Resource containerResource = Resource.newInstance(
      containerPlacement.getNumProcesses() * processResource.getMemory(),
      containerPlacement.getNumProcesses() * processResource.getVirtualCores()
    );
    mockYarnClientImpl.useResource(containerPlacement.getHost(), containerResource);
    Set<ContainerPlacementStrategy.ContainerPlacement> containerPlacementSet =
        typeToPlacementsMap.get(containerType);
    if (containerPlacementSet == null) {
      containerPlacementSet = new HashSet<ContainerPlacementStrategy.ContainerPlacement>();
      typeToPlacementsMap.put(containerType, containerPlacementSet);
    }
    containerPlacementSet.add(containerPlacement);
  }

  private static void release(
      MockYarnClientImpl mockYarnClientImpl,
      Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToPlacementsMap,
      String containerType,
      ContainerPlacementStrategy.ContainerPlacement containerPlacement,
      Resource processResource) {
    Resource containerResource = Resource.newInstance(
        containerPlacement.getNumProcesses() * processResource.getMemory(),
        containerPlacement.getNumProcesses() * processResource.getVirtualCores()
    );
    mockYarnClientImpl.unuseResource(containerPlacement.getHost(), containerResource);
    Set<ContainerPlacementStrategy.ContainerPlacement> containerPlacementSet =
        typeToPlacementsMap.get(containerType);
    containerPlacementSet.remove(containerPlacement);
  }
}
