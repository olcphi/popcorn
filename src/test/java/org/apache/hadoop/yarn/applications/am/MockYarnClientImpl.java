package org.apache.hadoop.yarn.applications.am;

import org.apache.hadoop.yarn.api.records.NodeId;
import org.apache.hadoop.yarn.api.records.NodeReport;
import org.apache.hadoop.yarn.api.records.NodeState;
import org.apache.hadoop.yarn.api.records.Resource;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.client.api.impl.YarnClientImpl;
import org.apache.hadoop.yarn.exceptions.YarnException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by v-yihhe on 1/6/2016.
 */
public class MockYarnClientImpl extends YarnClientImpl {

  private Map<String, Resource> hostToUsedMap = new HashMap<String, Resource>();

  private Map<String, Resource> hostToCapabilityMap = new HashMap<String, Resource>();

  public MockYarnClientImpl(String[] hosts, Resource[] used, Resource[] capability) {
    for (int idx = 0; idx != hosts.length; ++idx) {
      hostToUsedMap.put(hosts[idx], used[idx]);
      hostToCapabilityMap.put(hosts[idx], capability[idx]);
    }
  }

  @Override
  public List<NodeReport> getNodeReports(NodeState... states) throws YarnException, IOException {
    List<NodeReport> nodeReports = new ArrayList<NodeReport>();
    for (String host : hostToCapabilityMap.keySet()) {
      Resource used = hostToUsedMap.get(host);
      Resource capability = hostToCapabilityMap.get(host);
      nodeReports.add(setupNodeReport(
          host,
          Resource.newInstance(used.getMemory(), used.getVirtualCores()),
          Resource.newInstance(capability.getMemory(), capability.getVirtualCores())));
    }
    return nodeReports;
  }

  public void useResource(String host, Resource resource) {
    Resource used = hostToUsedMap.get(host);
    int newMemory = used.getMemory() + resource.getMemory();
    int newVCores = used.getVirtualCores() + resource.getVirtualCores();
    hostToUsedMap.put(host, Resource.newInstance(newMemory, newVCores));
  }

  public void unuseResource(String host, Resource resource) {
    Resource used = hostToUsedMap.get(host);
    int newMemory = used.getMemory() - resource.getMemory();
    int newVCores = used.getVirtualCores() - resource.getVirtualCores();
    hostToUsedMap.put(host, Resource.newInstance(newMemory, newVCores));
  }

  private static NodeReport setupNodeReport(String host, Resource used, Resource capability) {
    NodeId nodeId = NodeId.newInstance(host, 0);
    NodeReport nodeReport = NodeReport.newInstance(
        nodeId,
        NodeState.RUNNING,
        null,
        null,
        used,
        capability,
        0,
        null,
        0,
        null);
    return nodeReport;
  }

}
