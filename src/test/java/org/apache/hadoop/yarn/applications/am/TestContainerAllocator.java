package org.apache.hadoop.yarn.applications.am;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.yarn.api.records.*;
import org.apache.hadoop.yarn.applications.conf.AMConf;
import org.apache.hadoop.yarn.applications.conf.ContainerConf;
import org.apache.hadoop.yarn.client.api.AMRMClient;
import org.junit.*;

import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by v-yihhe on 1/21/2016.
 */
public class TestContainerAllocator {

  private static Configuration conf;

  private AMRMClient amrmClient;

  private MockYarnClientImpl mockYarnClientImpl;

  private ApplicationAttemptId appAttemptId = ApplicationAttemptId.newInstance(ApplicationId.newInstance(0, 0), 0);

  private Resource resource;

  private Integer containerNum = 0;

  @BeforeClass
  public static void beforeClass() {
    conf = new Configuration();
    conf.set("yarn.scheduler.minimum-allocation-mb", "1024");
    conf.set("yarn.scheduler.maximum-allocation-mb", "8192");
    conf.set("yarn.scheduler.minimum-allocation-vcores", "1");
    conf.set("yarn.scheduler.maximum-allocation-vcores", "8");
    conf.set("popcorn.yarn.launchContainers", "false");
  }

  @Before
  public void beforeTest() {
    // setup nodeReports
    String[] hosts = new String[4];
    Resource[] used = new Resource[4];
    Resource[] capability = new Resource[4];
    for (int num = 1; num <= 4; ++num) {
      hosts[num - 1] = "host" + num;
      used[num - 1] = Resource.newInstance(1024, 1);
      capability[num - 1] = Resource.newInstance(2048, 2);
    }

    mockYarnClientImpl = new MockYarnClientImpl(hosts, used, capability);

    resource = Resource.newInstance(1024, 1);

    amrmClient = AMRMClient.createAMRMClient();
    amrmClient.init(conf);
    amrmClient.start();
  }

  @Test
  public void noEnoughResourceForUpdateResourceRequest() {
    AMConf aMConf = new AMConf();
    List<List<String>> exclusions = new ArrayList<List<String>>();
    aMConf.setLaunchOrder(Arrays.asList("container"));
    Map<String, ContainerConf> containerConfs = new HashMap<String, ContainerConf>();
    aMConf.setContainerConfs(containerConfs);

    ContainerConf containerConf = new ContainerConf();
    containerConf.setNumProcesses(1);
    containerConf.setProcessMemory(2048);
    containerConf.setProcessVCores(2);

    containerConfs.put("container", containerConf);

    ContainerAllocator containerAllocator = new ContainerAllocator(
        conf,
        amrmClient,
        mockYarnClientImpl,
        aMConf,
        null,
        null,
        ByteBuffer.allocate(1)
    );

    Map<String, AtomicInteger> numRequestingProcessesMap =
        containerAllocator.getNumRequestingProcessesMap();

    containerAllocator.updateResourceRequests();
    Assert.assertEquals(0, numRequestingProcessesMap.get("container").get());
    containerAllocator.updateState();
    Assert.assertEquals(ContainerAllocator.State.UNSTARTED, containerAllocator.getState());
  }

  @Test
  public void normalUpdateResourceReqeuest() {

    AMConf aMConf = new AMConf();
    List<List<String>> exclusions = new ArrayList<List<String>>();
    exclusions.add(Arrays.asList("server", "server"));
    exclusions.add(Arrays.asList("worker", "worker"));
    aMConf.setExclusions(exclusions);
    aMConf.setLaunchOrder(Arrays.asList("server", "worker"));
    Map<String, ContainerConf> containerConfs = new HashMap<String, ContainerConf>();
    aMConf.setContainerConfs(containerConfs);

    ContainerConf serverContainerConf = new ContainerConf();
    containerConfs.put("server", serverContainerConf);
    serverContainerConf.setRecoverable(true);
    serverContainerConf.setNumProcesses(2);
    serverContainerConf.setProcessMemory(1024);
    serverContainerConf.setProcessVCores(1);

    ContainerConf workerContainerConf = new ContainerConf();
    containerConfs.put("worker", workerContainerConf);
    workerContainerConf.setRecoverable(true);
    workerContainerConf.setNumProcesses(2);
    workerContainerConf.setProcessMemory(1024);
    workerContainerConf.setProcessVCores(1);

    ContainerAllocator containerAllocator = new ContainerAllocator(
        conf,
        amrmClient,
        mockYarnClientImpl,
        aMConf,
        null,
        null,
        ByteBuffer.allocate(1)
    );
    Map<String, AtomicInteger> numRequestingProcessesMap = containerAllocator.getNumRequestingProcessesMap();
    Map<AMRMClient.ContainerRequest, String> requestToTypeMap =
        containerAllocator.getRequestToTypeMap();
    Map<AMRMClient.ContainerRequest, ContainerPlacementStrategy.ContainerPlacement> requestToPlacementMap =
        containerAllocator.getRequestToPlacementMap();
    Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToNewPlacementsMap =
        containerAllocator.getTypeToPendingPlacementsMap();
    containerAllocator.updateResourceRequests();
    Assert.assertEquals(2, numRequestingProcessesMap.get("server").get());
    Assert.assertEquals(0, numRequestingProcessesMap.get("worker").get());

    Assert.assertEquals(2, requestToTypeMap.size());
    Assert.assertEquals(2, requestToPlacementMap.size());
    for (AMRMClient.ContainerRequest containerRequest : requestToTypeMap.keySet()) {
      String containerType = requestToTypeMap.get(containerRequest);
      ContainerPlacementStrategy.ContainerPlacement containerPlacement =
          requestToPlacementMap.get(containerRequest);
      Assert.assertEquals("server", containerType);
      Assert.assertEquals(containerPlacement.getHost(), containerRequest.getNodes().get(0));
    }
    Assert.assertEquals(0, typeToNewPlacementsMap.get("server").size());
    Assert.assertEquals(2, typeToNewPlacementsMap.get("worker").size());
    containerAllocator.updateState();
    Assert.assertEquals(ContainerAllocator.State.ALLOCATING, containerAllocator.getState());

    List<ContainerPlacementStrategy.ContainerPlacement> placements =
            new ArrayList<ContainerPlacementStrategy.ContainerPlacement>(requestToPlacementMap.values());
    for (ContainerPlacementStrategy.ContainerPlacement placement : placements) {
      String host = placement.getHost();
      mockYarnClientImpl.useResource(host, resource);
      containerAllocator.handleAllocatedContainers(Arrays.asList(createContainer(host)));
    }
    // Wait for launching
    while (containerAllocator.getNumRunningProcessesMap().get("server").get() != 2) {
      try {
        Thread.sleep(100);
      } catch (Exception exp) {
      }
    }
    containerAllocator.updateResourceRequests();
    Assert.assertEquals(0, numRequestingProcessesMap.get("server").get());
    Assert.assertEquals(2, numRequestingProcessesMap.get("worker").get());
    Assert.assertEquals(0, typeToNewPlacementsMap.get("server").size());
    Assert.assertEquals(0, typeToNewPlacementsMap.get("worker").size());
  }

  @Test
  public void containerConfWithRecovery() {

    AMConf aMConf = new AMConf();
    aMConf.setLaunchOrder(Arrays.asList("process"));
    Map<String, ContainerConf> containerConfs = new HashMap<String, ContainerConf>();
    aMConf.setContainerConfs(containerConfs);

    ContainerConf containerConf = new ContainerConf();
    containerConf.setRecoverable(true);
    containerConf.setWaitForCompletion(true);
    containerConfs.put("process", containerConf);
    containerConf.setNumProcesses(4);
    containerConf.setProcessMemory(1024);
    containerConf.setProcessVCores(1);

    ContainerAllocator containerAllocator = new ContainerAllocator(
        conf,
        amrmClient,
        mockYarnClientImpl,
        aMConf,
        null,
        null,
        ByteBuffer.allocate(1)
    );
    containerAllocator.updateResourceRequests();
    Map<AMRMClient.ContainerRequest, ContainerPlacementStrategy.ContainerPlacement> requestToPlacementMap =
        containerAllocator.getRequestToPlacementMap();
    Assert.assertEquals(4, requestToPlacementMap.size());
    for (int hostNum = 1; hostNum <= 4; ++hostNum) {
      mockYarnClientImpl.useResource("host" + hostNum, resource);
      containerAllocator.handleAllocatedContainers(Arrays.asList(createContainer("host" + hostNum)));
    }
    // Wait for launching
    while (containerAllocator.getNumRunningProcessesMap().get("process").get() != 4) {
      try {
        Thread.sleep(100);
      } catch (Exception exp) {
      }
    }
    containerAllocator.updateState();
    Assert.assertEquals(ContainerAllocator.State.EXECUTING, containerAllocator.getState());
    ContainerId failedContainerId = containerAllocator.getContainerIdToPlacementMap().keySet().iterator().next();
    String failedhost = containerAllocator.getContainerIdToPlacementMap().get(failedContainerId).getHost();
    mockYarnClientImpl.unuseResource(failedhost, resource);
    containerAllocator.handleCompletedContainers(Arrays.asList(createFailedContainerStatus(failedContainerId)));
    Assert.assertEquals(
        FinalApplicationStatus.UNDEFINED,
        containerAllocator.getFinalApplicationResult().getFinalApplicationStatus());
    containerAllocator.updateState();
    Assert.assertEquals(ContainerAllocator.State.ALLOCATING, containerAllocator.getState());
    containerAllocator.updateResourceRequests();
    Assert.assertEquals(1, requestToPlacementMap.size());
  }

  @Test
  public void containerConfWithFlexibility() {

    AMConf aMConf = new AMConf();
    aMConf.setLaunchOrder(Arrays.asList("process"));
    Map<String, ContainerConf> containerConfs = new HashMap<String, ContainerConf>();
    aMConf.setContainerConfs(containerConfs);

    ContainerConf containerConf = new ContainerConf();
    containerConf.setFlexible(true);
    containerConfs.put("process", containerConf);
    containerConf.setNumProcesses(4);
    containerConf.setProcessMemory(1024);
    containerConf.setProcessVCores(1);

    ContainerAllocator containerAllocator = new ContainerAllocator(
        conf,
        amrmClient,
        mockYarnClientImpl,
        aMConf,
        null,
        null,
        null
    );
    containerAllocator.updateResourceRequests();
    Map<AMRMClient.ContainerRequest, ContainerPlacementStrategy.ContainerPlacement> requestToPlacementMap =
        containerAllocator.getRequestToPlacementMap();
    Assert.assertEquals(1, requestToPlacementMap.size());
    ContainerPlacementStrategy.ContainerPlacement containerPlacement =
        requestToPlacementMap.values().iterator().next();
    Assert.assertEquals(Integer.valueOf(1), containerPlacement.getNumProcesses());
    mockYarnClientImpl.useResource(containerPlacement.getHost(), resource);
    containerAllocator.handleAllocatedContainers(Arrays.asList(createContainer(containerPlacement.getHost())));
    // Wait for launching
    while (containerAllocator.getNumRunningProcessesMap().get("process").get() != 1) {
      try {
       Thread.sleep(100);
      } catch (Exception exp) {
      }
    }
    Assert.assertEquals(0, requestToPlacementMap.size());
    containerAllocator.updateState();
    Assert.assertEquals(ContainerAllocator.State.EXECUTING, containerAllocator.getState());
    containerAllocator.updateResourceRequests();
    Assert.assertEquals(3, requestToPlacementMap.size());
    List<Container> allocatedContainers = new ArrayList<Container>();
    for (ContainerPlacementStrategy.ContainerPlacement placement : requestToPlacementMap.values()) {
      Assert.assertEquals(Integer.valueOf(1), placement.getNumProcesses());
      mockYarnClientImpl.useResource(containerPlacement.getHost(), resource);
      allocatedContainers.add(createContainer(placement.getHost()));
    }
    containerAllocator.handleAllocatedContainers(allocatedContainers);
    Assert.assertEquals(0, requestToPlacementMap.size());
    // Wait for launching
    while (containerAllocator.getNumRunningProcessesMap().get("process").get() != 4) {
      try {
        Thread.sleep(100);
      } catch (Exception exp) {
      }
    }
  }

  @Test
  public void containerConfWithFlexibilityRemoveAllRequestsAfterFailure() {

    AMConf aMConf = new AMConf();
    List<List<String>> exclusions = new ArrayList<List<String>>();
    exclusions.add(Arrays.asList("server", "server"));
    exclusions.add(Arrays.asList("worker", "worker"));
    aMConf.setLaunchOrder(Arrays.asList("server", "worker"));
    Map<String, ContainerConf> containerConfs = new HashMap<String, ContainerConf>();
    aMConf.setContainerConfs(containerConfs);

    ContainerConf serverContainerConf = new ContainerConf();
    containerConfs.put("server", serverContainerConf);
    serverContainerConf.setRecoverable(true);
    serverContainerConf.setNumProcesses(1);
    serverContainerConf.setProcessMemory(1024);
    serverContainerConf.setProcessVCores(1);

    ContainerConf workerContainerConf = new ContainerConf();
    containerConfs.put("worker", workerContainerConf);
    workerContainerConf.setFlexible(true);
    workerContainerConf.setNumProcesses(2);
    workerContainerConf.setProcessMemory(1024);
    workerContainerConf.setProcessVCores(1);

    ContainerAllocator containerAllocator = new ContainerAllocator(
        conf,
        amrmClient,
        mockYarnClientImpl,
        aMConf,
        null,
        null,
        ByteBuffer.allocate(1)
    );
    containerAllocator.updateResourceRequests();
    containerAllocator.updateState();
    Assert.assertEquals(ContainerAllocator.State.ALLOCATING, containerAllocator.getState());
    Map<AMRMClient.ContainerRequest, ContainerPlacementStrategy.ContainerPlacement> requestToPlacementMap =
        containerAllocator.getRequestToPlacementMap();
    Assert.assertEquals(1, requestToPlacementMap.size());
    ContainerPlacementStrategy.ContainerPlacement placement = requestToPlacementMap.values().iterator().next();
    Container serverContainer = createContainer(placement.getHost());
    mockYarnClientImpl.useResource(placement.getHost(), resource);
    containerAllocator.handleAllocatedContainers(Arrays.asList(serverContainer));
    // Wait for launching
    while (containerAllocator.getNumRunningProcessesMap().get("server").get() != 1) {
      try {
        Thread.sleep(100);
      } catch (Exception exp) {
      }
    }
    containerAllocator.updateResourceRequests();
    Assert.assertEquals(1, requestToPlacementMap.size());
    placement = requestToPlacementMap.values().iterator().next();
    Container workerContainer = createContainer(placement.getHost());
    mockYarnClientImpl.useResource(workerContainer.getNodeId().getHost(), resource);
    containerAllocator.handleAllocatedContainers(Arrays.asList(workerContainer));
    // Wait for launching
    while (containerAllocator.getNumRunningProcessesMap().get("worker").get() != 1) {
      try {
        Thread.sleep(100);
      } catch (Exception exp) {
      }
    }
    containerAllocator.updateState();
    Assert.assertEquals(ContainerAllocator.State.EXECUTING, containerAllocator.getState());
    containerAllocator.updateResourceRequests();
    Assert.assertEquals(1, requestToPlacementMap.size());
    ContainerStatus failedContainerStatus = createFailedContainerStatus(serverContainer.getId());
    String failedhost = serverContainer.getNodeId().getHost();
    mockYarnClientImpl.unuseResource(failedhost, resource);
    containerAllocator.handleCompletedContainers(Arrays.asList(failedContainerStatus));
    Assert.assertEquals(0, requestToPlacementMap.size());
    containerAllocator.updateState();
    Assert.assertEquals(ContainerAllocator.State.ALLOCATING, containerAllocator.getState());
    containerAllocator.updateResourceRequests();
    Map<AMRMClient.ContainerRequest, String> requestToTypeMap = containerAllocator.getRequestToTypeMap();
    Assert.assertEquals(1, requestToPlacementMap.size());
    Assert.assertEquals(1, requestToTypeMap.size());
    String containerType = requestToTypeMap.values().iterator().next();
    Assert.assertEquals("server", containerType);
  }

  @After
  public void afterTest() {
    amrmClient.stop();
  }

  private Container createContainer(String host) {
    ContainerId containerId = ContainerId.newContainerId(appAttemptId, containerNum);
    containerNum += 1;
    NodeId nodeId = NodeId.newInstance(host, 1000);
    return Container.newInstance(containerId, nodeId, "", resource, Priority.newInstance(1), null);
  }

  private ContainerStatus createFailedContainerStatus(ContainerId containerId) {
    return ContainerStatus.newInstance(containerId, ContainerState.COMPLETE, "", 1);
  }
}
