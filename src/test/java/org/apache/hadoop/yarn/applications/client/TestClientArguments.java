package org.apache.hadoop.yarn.applications.client;

import org.apache.commons.cli.ParseException;
import org.apache.hadoop.yarn.applications.constants.ClientConstants;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by v-yihhe on 1/4/2016.
 */
public class TestClientArguments {
  @Test
  public void parseHelpArgTest() {
    String[] args = new String[] {"-" + ClientConstants.OPT_HELP};
    ClientArguments clientArguments = null;
    try {
     clientArguments = ClientArguments.parseArgs(args);
    } catch(ParseException exp) {
      Assert.fail();
    }
    Assert.assertTrue(clientArguments.getPrintHelp());
  }

  @Test
  public void parseSomeArgsTest() {
    String[] args = new String[] {
        "-" + ClientConstants.OPT_CONFIG,
        "config.json"};
    ClientArguments clientArguments = null;
    try {
      clientArguments = ClientArguments.parseArgs(args);
    } catch(ParseException exp) {
      Assert.fail();
    }
    Assert.assertEquals(clientArguments.getConfigFile(), "config.json");
  }
}
