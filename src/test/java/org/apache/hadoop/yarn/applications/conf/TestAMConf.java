package org.apache.hadoop.yarn.applications.conf;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by v-yihhe on 1/4/2016.
 */
public class TestAMConf {
  @Test
  public void parseJsonAndArgsTest() {
    Map<String, ContainerConf> containerConfs = new HashMap<String, ContainerConf>();
    ContainerConf containerConf = new ContainerConf();
    containerConf.setNumProcesses(1);
    containerConf.setProcessMemory(512);
    containerConf.setProcessVCores(1);
    List<String> resourcePaths = new ArrayList<String>();
    resourcePaths.add("");
    containerConf.setResourcePaths(resourcePaths);
    Map<String, String> envVars = new HashMap<String, String>();
    envVars.put("NUM_PROCESSES", "1");
    containerConf.setEnvVars(envVars);
    containerConfs.put("worker", containerConf);
    AMConf aMConf = new AMConf();
    aMConf.setContainerConfs(containerConfs);
    Gson gson = new Gson();
    Map<String, Object> confMap = gson.fromJson(gson.toJson(aMConf), HashMap.class);
    Map<String, String> arguments = new HashMap<String, String>();
    arguments.put("containerConfs/worker/numProcesses", "n");
    arguments.put("containerConfs/worker/envVars/NUM_PROCESSES", "n");
    arguments.put("containerConfs/worker/processMemory", "m");
    arguments.put("containerConfs/worker/processVCores", "c");
    arguments.put("containerConfs/worker/resourcePaths/0", "r");
    confMap.put("arguments", arguments);
    String json = gson.toJson(confMap);
    String[] args = new String[] {
        "-n",
        "10",
        "-m",
        "1024",
        "-c",
        "2",
        "-r",
        "resources"};
    ContainerConf actualContainerConf = null;
    List<String> actualResourcePaths = null;
    try {
      AMConf actualAMConf = AMConf.parseArgsAndJson(args, json);
      actualContainerConf = actualAMConf.getContainerConfs().get("worker");
      actualResourcePaths = actualContainerConf.getResourcePaths();
    } catch (Exception exp) {
      Assert.fail();
    }
    Assert.assertEquals("resources", actualResourcePaths.get(0));
    Assert.assertEquals(Integer.valueOf(10), actualContainerConf.getNumProcesses());
    Assert.assertEquals("10", actualContainerConf.getEnvVars().get("NUM_PROCESSES"));
    Assert.assertEquals(Integer.valueOf(1024), actualContainerConf.getProcessMemory());
    Assert.assertEquals(Integer.valueOf(2), actualContainerConf.getProcessVCores());
  }
}
