package org.apache.hadoop.yarn.applications.am;

import com.google.gson.Gson;
import com.sun.istack.NotNull;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.yarn.api.protocolrecords.AllocateResponse;
import org.apache.hadoop.yarn.api.records.*;
import org.apache.hadoop.yarn.applications.conf.AMConf;
import org.apache.hadoop.yarn.applications.conf.ContainerConf;
import org.apache.hadoop.yarn.applications.constants.AMConstants;
import org.apache.hadoop.yarn.applications.util.MachineListFormatter;
import org.apache.hadoop.yarn.applications.util.NameUtils;
import org.apache.hadoop.yarn.client.api.AMRMClient;
import org.apache.hadoop.yarn.client.api.YarnClient;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by v-yihhe on 12/16/2015.
 */
public class ContainerAllocator {

  private static final Log LOG = LogFactory.getLog(ContainerAllocator.class);

  private final static int RM_REQUEST_PRIORITY = 1;

  private Configuration conf;

  private AMRMClient amrmClient;

  private AMConf aMConf;

  private Map<String, String> genFileToPath = new HashMap<String, String>();

  private String hdfsAppDir;

  private ContainerPlacementStrategy containerPlacementStrategy;

  private FinalApplicationResult finalApplicationResult;

  private Map<String, Integer> typeToRankCounterMap =
      new HashMap<String, Integer>();

  private Integer launchIndex = 0;

  private Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToPendingPlacementsMap =
      new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();

  // Record container requests' information

  private Map<AMRMClient.ContainerRequest, String> requestToTypeMap =
      new HashMap<AMRMClient.ContainerRequest, String>();

  private Map<AMRMClient.ContainerRequest, ContainerPlacementStrategy.ContainerPlacement> requestToPlacementMap =
      new HashMap<AMRMClient.ContainerRequest, ContainerPlacementStrategy.ContainerPlacement>();

  // Record allocated containers' information

  private Map<ContainerId, String> containerIdToTypeMap = new HashMap<ContainerId, String>();

  private Map<ContainerId, ContainerPlacementStrategy.ContainerPlacement> containerIdToPlacementMap =
      new HashMap<ContainerId, ContainerPlacementStrategy.ContainerPlacement>();

  private Map<ContainerPlacementStrategy.ContainerPlacement, Map<String, String>> placementToGenMap =
      new HashMap<ContainerPlacementStrategy.ContainerPlacement, Map<String, String>>();

  private Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToCurrentPlacementsMap =
      new HashMap<String, Set<ContainerPlacementStrategy.ContainerPlacement>>();

  private ExecutorService launcherPool = Executors.newFixedThreadPool(30);

  // Processes in container will go through three stages : requesting from RM, launching on NM and running.
  // So use three map to profile processes of each stage.

  private Map<String, AtomicInteger> numRunningProcessesMap = new HashMap<String, AtomicInteger>();

  private Map<String, AtomicInteger> numLaunchingProcessesMap = new HashMap<String, AtomicInteger>();

  private Map<String, AtomicInteger> numRequestingProcessesMap = new HashMap<String, AtomicInteger>();

  private Map<String, Integer> numRequiredProcessesMap = new HashMap<String, Integer>();

  private FileSystem fs;

  private Integer receiverPort;

  private ByteBuffer allTokens;

  private State state = State.UNSTARTED;

  private List<Integer> prevStatus = new ArrayList<Integer>();

  private boolean hasGeneratedMachineList = false;

  public ContainerAllocator(
      Configuration conf,
      AMRMClient amrmClient,
      YarnClient yarnClient,
      AMConf aMConf,
      String hdfsAppDir,
      Integer receiverPort,
      ByteBuffer allTokens) {
    this.conf = conf;
    this.amrmClient = amrmClient;
    this.aMConf = aMConf;
    this.hdfsAppDir = hdfsAppDir;
    this.receiverPort = receiverPort;
    this.finalApplicationResult = new FinalApplicationResult();
    this.allTokens = allTokens;

    for (String containerType : aMConf.getLaunchOrder()) {
      this.numRequestingProcessesMap.put(containerType, new AtomicInteger(0));
      this.numLaunchingProcessesMap.put(containerType, new AtomicInteger(0));
      this.numRunningProcessesMap.put(containerType, new AtomicInteger(0));
      ContainerConf containerConf = aMConf.getContainerConfs().get(containerType);
      this.numRequiredProcessesMap.put(containerType, containerConf.getNumProcesses());
      this.typeToRankCounterMap.put(containerType, 0);
      this.typeToPendingPlacementsMap.put(containerType, new HashSet<ContainerPlacementStrategy.ContainerPlacement>());
      this.typeToCurrentPlacementsMap.put(containerType, new HashSet<ContainerPlacementStrategy.ContainerPlacement>());
    }

    // Initialize containerPlacementStrategy

    this.containerPlacementStrategy = new ContainerPlacementStrategy(conf, yarnClient, aMConf.getExclusions());

    try {
      fs = FileSystem.get(conf);
    } catch (IOException exp) {
    }

  }

  /**
   * This method is used to generate container requests, get allocated resources from RM and launch allocated containers.
   */
  public synchronized void allocateResources() {
    if (state == State.EXITED) {
      return;
    }
    updateResourceRequests();
    interactWithRM();
    updateState();
  }

  /**
   * Remove all state of container allocator, removing all container requests and releasing all allocated containers.
   * After clear, container allocator should recover to the initial states.
   */
  public synchronized void clear() {
    state = state.CLEARING;
    for (String containerType : aMConf.getLaunchOrder()) {
      Set<ContainerPlacementStrategy.ContainerPlacement> placementSet =
          typeToPendingPlacementsMap.get(containerType);
      placementSet.clear();
    }
    removeAllRequests();
    while (true) {
      LOG.info("Waiting for clearing conatiner allocator");
      interactWithRM();
      boolean isClear = true;
      boolean isAllLaunched = true;
      for (String containerType : aMConf.getLaunchOrder()) {
        int currentNumLaunchingProcesses = numLaunchingProcessesMap.get(containerType).get();
        int currentNumRunningProcesses = numRunningProcessesMap.get(containerType).get();
        LOG.info(String.format(
            "Checking %s's status, numLaunchingProcesses = %d, numRunningProcesses = %d",
            containerType,
            currentNumLaunchingProcesses,
            currentNumRunningProcesses));
        isAllLaunched = isAllLaunched && currentNumLaunchingProcesses == 0;
        isClear = isClear && currentNumLaunchingProcesses == 0 && currentNumRunningProcesses == 0;
      }
      if (isClear) {
        LOG.info("All container requests and allocated containers are clear.");
        break;
      } else if (isAllLaunched) {
        // if releasing launching containers, AM will fail. So in order to avoid it happen, AM need to
        // wait for containers to finish launching.
        LOG.info("All allocated containers are launched, start releasing.");
        for (ContainerId containerId : containerIdToPlacementMap.keySet()) {
          LOG.info(String.format(
              "Releasing allocated container %s for exiting.",
              containerId.toString()));
          amrmClient.releaseAssignedContainer(containerId);
        }
      }
      try {
        Thread.sleep(1000);
      } catch (Exception exp) {
      }
    }
    for (String containerType : aMConf.getLaunchOrder()) {
      ContainerConf containerConf = aMConf.getContainerConfs().get(containerType);
      this.numRequiredProcessesMap.put(containerType, containerConf.getNumProcesses());
      this.typeToRankCounterMap.put(containerType, 0);
    }
    state = State.UNSTARTED;
    hasGeneratedMachineList = false;
    launchIndex = 0;
  }

  /**
   * Setup final application result and clear container allocator, finally set state to EXITED
   * @param appMessage
   * @param finalApplicationStatus
   * @param trackingUrl
   */
  public synchronized void finish(
      String appMessage,
      FinalApplicationStatus finalApplicationStatus,
      String trackingUrl) {
    finalApplicationResult.setAppMessage(appMessage);
    finalApplicationResult.setFinalApplicationStatus(finalApplicationStatus);
    finalApplicationResult.setTrackingUrl(trackingUrl);
    clear();
    state = State.EXITED;
  }

  /**
   * Generate container requests according to current status of container allocator. When some containers have not
   * been allocated or some containers are launching, the method will do nothing. When it try to generate container
   * requests, it first generates container placement requests by checking the condition of all container types.
   * Then, it use ContainerPlacementStrategy to turn container placement requests to container placements.
   * When it first generates the container placements, it will generate the machine_list of container types
   * without flexibility and recovery. The container placements are converted to
   */
  public synchronized void updateResourceRequests() {
    LOG.debug("Updating resource requests");
    List<String> order = aMConf.getLaunchOrder();
    List<Integer> currentStatus = new ArrayList<Integer>();
    for (String containerType : order) {
        currentStatus.add(numRequestingProcessesMap.get(containerType).get());
        currentStatus.add(numLaunchingProcessesMap.get(containerType).get());
        currentStatus.add(numRunningProcessesMap.get(containerType).get());
        currentStatus.add(numRequiredProcessesMap.get(containerType));
    }
    // Update containers' statuses when they changed.
    if (!prevStatus.equals(currentStatus)) {
        Integer idx = 0;
        for (String containerType : order) {
            LOG.info(String.format(
                    "Current status of %s : numRequestingProcesses = %d, numLaunchingProcesses = %d, " +
                            "numRunningProcesses = %d, numRequiredProcesses = %d",
                    containerType,
                    currentStatus.get(idx),
                    currentStatus.get(idx + 1),
                    currentStatus.get(idx + 2),
                    currentStatus.get(idx + 3)));
            idx += 4;
        }
    }
    boolean shouldWait = false;
    for (Integer idx = 0; idx != order.size(); ++idx) {
      Integer numRequestingProcesses = currentStatus.get(idx * 4);
      Integer numLaunchingProcesses = currentStatus.get(idx * 4 + 1);
      if (numRequestingProcesses > 0 || numLaunchingProcesses > 0) {
        shouldWait = true;
        break;
      }
    }
    prevStatus = currentStatus;
    if (shouldWait) {
      LOG.debug("Wait for requesting or launching processes, no update");
      return;
    }
    // Update typeToPendingPlacementsMap
    Map<String, ContainerPlacementStrategy.ContainerPlacementRequest> containerPlacementRequestMap =
        new HashMap<String, ContainerPlacementStrategy.ContainerPlacementRequest>();
    for (String containerType : order) {
      ContainerConf containerConf = aMConf.getContainerConfs().get(containerType);
      AtomicInteger numRunningProcesses = numRunningProcessesMap.get(containerType);
      Integer numPendingProcesses = 0;
      for (ContainerPlacementStrategy.ContainerPlacement placement : typeToPendingPlacementsMap.get(containerType)) {
        numPendingProcesses += placement.getNumProcesses();
      }
      Integer numRequiredProcesses = numRequiredProcessesMap.get(containerType);
      // Figure out how many processes should be requested
      if (numRunningProcesses.get() + numPendingProcesses < numRequiredProcesses) {
        int numRequestProcesses = 0;
        switch (state) {
          case UNSTARTED:
          case ALLOCATING:
            if (!containerConf.getFlexible()) {
              numRequestProcesses = numRequiredProcesses - numRunningProcesses.get() - numPendingProcesses;
            } else if (numRunningProcesses.get() + numPendingProcesses == 0) {
              numRequestProcesses = 1;
            }
            break;
          case EXECUTING:
            numRequestProcesses = numRequiredProcesses - numRunningProcesses.get() - numPendingProcesses;
            break;
        }
        if (numRequestProcesses > 0) {
          boolean multipleProcessesPerContainer = containerConf.getMultipleProcessesPerContainer();
          Resource needResource = Resource.newInstance(
              containerConf.getProcessMemory(),
              containerConf.getProcessVCores()
          );
          ContainerPlacementStrategy.ContainerPlacementRequest containerPlacementRequest =
              new ContainerPlacementStrategy.ContainerPlacementRequest(
                  numRequestProcesses,
                  multipleProcessesPerContainer,
                  needResource
              );
          containerPlacementRequestMap.put(containerType, containerPlacementRequest);
        }
      }
    }

    // ContainerPlacementRequest to ContainerPlacement
    if (!containerPlacementRequestMap.isEmpty()) {
      LOG.info(String.format(
          "Deal with new container placement requests, containerPlacementRequestMap = %s",
          new Gson().toJson(containerPlacementRequestMap)));
      Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> newPlacements =
          containerPlacementStrategy.generatePlacements(
              typeToPendingPlacementsMap,
              typeToCurrentPlacementsMap,
              containerPlacementRequestMap);
      if (newPlacements == null) {
        LOG.info("AM failed to find a feasible container placements.");
        return;
      }
      LOG.info(String.format(
          "get newPlacements = %s",
          new Gson().toJson(newPlacements)));
      for (Map.Entry<String, Set<ContainerPlacementStrategy.ContainerPlacement>> typeToPlacements :
          newPlacements.entrySet()) {
        String containerType = typeToPlacements.getKey();
        Set<ContainerPlacementStrategy.ContainerPlacement> placements = typeToPlacements.getValue();
        for (ContainerPlacementStrategy.ContainerPlacement containerPlacement : placements) {
          Set<ContainerPlacementStrategy.ContainerPlacement> placementSet =
              typeToPendingPlacementsMap.get(containerType);
          placementSet.add(containerPlacement);
          Map<String, String> genEnvToValue = setupGenEnvToValue(containerType, containerPlacement);
          placementToGenMap.put(containerPlacement, genEnvToValue);
        }
      }
      if (!hasGeneratedMachineList) {
        LOG.info("Start generating machine_list of all container types without flexibility and recovery.");
        for (String containerType : aMConf.getLaunchOrder()) {
          ContainerConf containerConf = aMConf.getContainerConfs().get(containerType);
          if (!containerConf.getRecoverable() && !containerConf.getFlexible()) {
            genMachineList(containerType);
          }
        }
        hasGeneratedMachineList = true;
      }
    }

    // ContainerPlacement to ContainerRequest
    boolean shouldIncreaseLaunchIndex = true;
    for (int idx = 0; idx <= launchIndex; ++idx) {
      String containerType = order.get(idx);
      Set<ContainerPlacementStrategy.ContainerPlacement> placementSet =
          typeToPendingPlacementsMap.get(containerType);
      ContainerConf containerConf = aMConf.getContainerConfs().get(containerType);
      AtomicInteger numRequestingProcesses = numRequestingProcessesMap.get(containerType);
      for (ContainerPlacementStrategy.ContainerPlacement containerPlacement : placementSet) {
        String host = containerPlacement.getHost();
        int numProcesses = containerPlacement.getNumProcesses();
        int memory = containerConf.getProcessMemory() * numProcesses;
        int vCores = containerConf.getProcessVCores() * numProcesses;
        numRequestingProcesses.addAndGet(numProcesses);
        AMRMClient.ContainerRequest containerRequest = setupContainerRequest(
            memory,
            vCores,
            host,
            RM_REQUEST_PRIORITY);
        requestToTypeMap.put(containerRequest, containerType);
        requestToPlacementMap.put(containerRequest, containerPlacement);
        LOG.info(String.format(
            "Add container request %sNodes[%s] to amrmClient",
            containerRequest.toString(),
            host));
        amrmClient.addContainerRequest(containerRequest);
      }
      placementSet.clear();
      AtomicInteger numRunningProcesses = numRunningProcessesMap.get(containerType);
      Integer numRequiredProcesses = numRequiredProcessesMap.get(containerType);
      shouldIncreaseLaunchIndex = shouldIncreaseLaunchIndex && numRunningProcesses.get() == numRequiredProcesses;
      if (shouldIncreaseLaunchIndex && idx == launchIndex && launchIndex != order.size() - 1) {
        ++launchIndex;
        shouldIncreaseLaunchIndex = false;
      }
    }
  }

  /**
   * update state of container allocator
   */
  public synchronized void updateState() {
    if (state == State.EXITED || state == State.UNSTARTED && !hasGeneratedMachineList) {
      return;
    }
    // Update the state by comparison between numRunningProcessses and numRequiredProcesses
    boolean isExecuting = true;
    for (String containerType : aMConf.getLaunchOrder()) {
      ContainerConf containerConf = aMConf.getContainerConfs().get(containerType);
      int currentNumRunningProcesses = numRunningProcessesMap.get(containerType).get();
      if (!containerConf.getFlexible() && currentNumRunningProcesses != numRequiredProcessesMap.get(containerType) ||
          containerConf.getFlexible() && currentNumRunningProcesses == 0) {
        isExecuting = false;
        break;
      }
    }
    state = isExecuting ? State.EXECUTING : State.ALLOCATING;
  }

  /**
   * handle allocated containers by matching them to container requests. For matched containers, just start them in
   * nodes. For unmatched containers, release them.
   * @param allocatedContainers
   */
  public synchronized void handleAllocatedContainers(List<Container> allocatedContainers) {
    LOG.info(String.format(
        "Handling allocated containers, size of allocatedContainers = %d",
        allocatedContainers.size()));
    List<Container> containersToUse = new ArrayList<Container>();
    List<Container> remaining = new ArrayList<Container>();
    // match by host
    for (Container allocatedContainer : allocatedContainers) {
      matchContainerToRequest(
          allocatedContainer,
          allocatedContainer.getNodeId().getHost(),
          containersToUse,
          remaining);
    }
    for (Container container : remaining) {
      LOG.info(String.format(
          "Releasing remainig container %s.",
          container.toString()));
      amrmClient.releaseAssignedContainer(container.getId());
    }
    runAllocatedContainers(containersToUse);
  }

  /**
   * handle completed containers according to the state of container allocator
   * @param completedContainers
   */
  public synchronized void handleCompletedContainers(List<ContainerStatus> completedContainers) {
    boolean isFailed = false;
    boolean removeUnnecessaryRequests = false;
    for (ContainerStatus containerStatus : completedContainers) {
      String containerType = containerIdToTypeMap.get(containerStatus.getContainerId());
      if (containerType == null) {
        LOG.warn(String.format(
            "Unknown container %s completed",
            containerStatus.getContainerId()));
        continue;
      }
      LOG.info(String.format(
          "ContainerType = %s, %s completed with exit status = %d \n%s",
          containerType,
          containerStatus.getContainerId(),
          containerStatus.getExitStatus(),
          containerStatus.getDiagnostics()));
      int exitStatus = containerStatus.getExitStatus();
      ContainerId containerId = containerStatus.getContainerId();
      ContainerConf containerConf = aMConf.getContainerConfs().get(containerType);
      Boolean recoverable = containerConf.getRecoverable();
      if (state != State.CLEARING && exitStatus != 0) {
        if (recoverable) {
          LOG.info(String.format(
              "The failure of %s's container is recoverable.",
              containerType));
        } else {
          LOG.info(String.format(
              "The failure of %s's container is not recoverable.",
              containerType));
          isFailed = true;
        }
      }
      containerIdToTypeMap.remove(containerId);
      ContainerPlacementStrategy.ContainerPlacement containerPlacement =
          containerIdToPlacementMap.remove(containerId);
      placementToGenMap.remove(containerPlacement);
      Set<ContainerPlacementStrategy.ContainerPlacement> containerPlacementSet =
          typeToCurrentPlacementsMap.get(containerType);
      containerPlacementSet.remove(containerPlacement);
      AtomicInteger numRunningProcesses = numRunningProcessesMap.get(containerType);
      numRunningProcesses.addAndGet(-containerPlacement.getNumProcesses());
      if (exitStatus == 0) {
        Integer numRequiredProcesses = numRequiredProcessesMap.get(containerType);
        numRequiredProcessesMap.put(containerType, numRequiredProcesses - containerPlacement.getNumProcesses());
      } else if (state == State.EXECUTING && containerConf.getRecoverable() &&
          (!containerConf.getFlexible() || numRunningProcesses.get() == 0)) {
        // Maybe there are some unnecessary container requests for program to execute,
        // so they should be removed.
        removeUnnecessaryRequests = true;
      }
    }
    if (isFailed) {
      finish(
          String.format(
              "Application failed. Due to container exit abnormally\n"),
          FinalApplicationStatus.FAILED,
          null);
    } else if (removeUnnecessaryRequests) {
      for (String containerType : aMConf.getLaunchOrder()) {
        Set<ContainerPlacementStrategy.ContainerPlacement> placementSet =
            typeToPendingPlacementsMap.get(containerType);
        placementSet.clear();
      }
      removeAllRequests();
    } else {
      boolean succeed = true;
      for (Map.Entry<String, ContainerConf> entry : aMConf.getContainerConfs().entrySet()) {
        String containerType = entry.getKey();
        ContainerConf containerConf = entry.getValue();
        if (containerConf.getWaitForCompletion() && numRequiredProcessesMap.get(containerType) > 0) {
          succeed = false;
          break;
        }
      }
      if (succeed) {
        finish(
            "Application Succeeded.",
            FinalApplicationStatus.SUCCEEDED,
            null);
      }
    }
  }

  /**
   * Put launch threads of allocated containers into thread pools
   * @param containersToUse
   */
  private void runAllocatedContainers(List<Container> containersToUse) {
    LOG.info(String.format(
        "Running allocated containers, size of containersToUse = %d",
        containersToUse.size()));
    for (Container container : containersToUse) {
      String containerType = containerIdToTypeMap.get(container.getId());
      ContainerPlacementStrategy.ContainerPlacement containerPlacement =
          containerIdToPlacementMap.get(container.getId());
      ContainerConf containerConf = aMConf.getContainerConfs().get(containerType);
      AtomicInteger numLaunchingProcesses = numLaunchingProcessesMap.get(containerType);
      AtomicInteger numRunningProcesses = numRunningProcessesMap.get(containerType);
      Map<String, String> genEnvToValue = placementToGenMap.get(containerPlacement);
      LaunchContainerRunnable launchContainerRunnable = new LaunchContainerRunnable(
          numLaunchingProcesses,
          numRunningProcesses,
          aMConf.getJarPath(),
          receiverPort,
          containerConf.getObserveOutput(),
          containerPlacement.getNumProcesses(),
          container,
          conf,
          containerType,
          containerConf,
          genFileToPath,
          genEnvToValue,
          allTokens);
      launcherPool.execute(launchContainerRunnable);
    }
  }

  /**
   * Since when RM allocate some containers to AM, AM doesn't know how allocated containers match container requests,
   * so container allocator should use method matchContainerToRequest to figure out which container request allocated
   * container matchs.
   * @param allocatedContainer the allocated container which try to match a container request in amrmclient
   * @param location host of the allocated container
   * @param containersToUse if the allocated container matches container request, it will be put in conatinersToUse
   * @param remaining if the allocated container doens't match container request it will be put in remaining, and will
   *                  be released later.
   */
  private void matchContainerToRequest(
      Container allocatedContainer,
      String location,
      List<Container> containersToUse,
      List<Container> remaining) {
    Resource matchingResource = Resource.newInstance(
        allocatedContainer.getResource().getMemory(),
        allocatedContainer.getResource().getVirtualCores());
    List<Collection<AMRMClient.ContainerRequest>> matchingRequests = amrmClient.getMatchingRequests(
        allocatedContainer.getPriority(),
        location,
        matchingResource);
    if (!matchingRequests.isEmpty()) {
      AMRMClient.ContainerRequest containerRequest = matchingRequests.get(0).iterator().next();
      String containerType = requestToTypeMap.get(containerRequest);
      ContainerPlacementStrategy.ContainerPlacement containerPlacement = requestToPlacementMap.get(containerRequest);
      AtomicInteger numRequestingProcesses = numRequestingProcessesMap.get(containerType);
      numRequestingProcesses.addAndGet(-containerPlacement.getNumProcesses());
      LOG.info(String.format(
          "Match allocated container %s to container request %s of %s",
          allocatedContainer.toString(),
          containerRequest.toString(),
          containerType));
      requestToTypeMap.remove(containerRequest);
      requestToPlacementMap.remove(containerRequest);
      amrmClient.removeContainerRequest(containerRequest);
      // Add placement to typeToCurrentPlacementsMap
      Set<ContainerPlacementStrategy.ContainerPlacement> containerPlacements =
          typeToCurrentPlacementsMap.get(containerType);
      containerPlacements.add(containerPlacement);

      ContainerId containerId = allocatedContainer.getId();
      // Add placement to containerIdToPlacementMap
      containerIdToPlacementMap.put(containerId, containerPlacement);
      containerIdToTypeMap.put(containerId, containerType);
      containersToUse.add(allocatedContainer);
    } else {
      remaining.add(allocatedContainer);
    }
  }

  /**
   * interact with resource manager, including sending containr requests and heartbeat, receiving
   * allocated containers and completed containers.
   */
  private void interactWithRM() {
    AllocateResponse allocateResponse;
    try {
      float progressIndicator = 0.1f;
      allocateResponse = amrmClient.allocate(progressIndicator);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new RuntimeException(exp.getMessage());
    }
    List<Container> allocatedContainers = allocateResponse.getAllocatedContainers();
    if (!allocatedContainers.isEmpty()) {
      handleAllocatedContainers(allocatedContainers);
    }
    List<ContainerStatus> completedContainers = allocateResponse.getCompletedContainersStatuses();
    if (!completedContainers.isEmpty()) {
      handleCompletedContainers(completedContainers);
    }
  }

  /**
   * remove all requests from amrmclient and the state of container allocator.
   */
  private void removeAllRequests() {
    LOG.info("Removing all container reqeusts.");
    // Remove requesting containers
    for (AMRMClient.ContainerRequest containerRequest : requestToPlacementMap.keySet()) {
      LOG.info(String.format(
          "Remove container request %sNodes[%s] to amrmClient",
          containerRequest.toString(),
          containerRequest.getNodes().get(0)));
      amrmClient.removeContainerRequest(containerRequest);
    }
    for (Map.Entry<String, AtomicInteger> entry : numRequestingProcessesMap.entrySet()) {
      entry.getValue().set(0);
    }
    requestToPlacementMap.clear();
    requestToTypeMap.clear();
  }

  /**
   * Generate machine_list of the containerType according to requestToTypeMap and requestToPlacementMap
   * @param containerType
   */
  private void genMachineList(String containerType) {
    LOG.info("Generating machine list of " + containerType + "'s containers");

    String machineListFilename = containerType + "_machine_list";
    Path machineListPath = new Path(hdfsAppDir + "/gen/" + machineListFilename);
    // If machine list has been generated, delete it and generate again.
    try {
      LOG.info(String.format(
          "Delete previous machine_list file of %s.",
          containerType));
      FileStatus fileStatus = fs.getFileStatus(machineListPath);
      if (fileStatus.isFile()) {
        fs.delete(machineListPath, true);
      }
    } catch (Exception exp) {
    }
    String machineListFormat = aMConf.getContainerConfs().get(containerType).getMachineListFormat();
    StringBuilder builder = new StringBuilder();
    Set<ContainerPlacementStrategy.ContainerPlacement> placements =
        typeToPendingPlacementsMap.get(containerType);
    ContainerConf containerConf = aMConf.getContainerConfs().get(containerType);
    Map<String, String> envVars = containerConf.getEnvVars();
    for (ContainerPlacementStrategy.ContainerPlacement placement : placements) {
      Map<String, String> genEnvToValue = placementToGenMap.get(placement);
      String formatString = MachineListFormatter.format(machineListFormat, genEnvToValue, envVars);
      builder.append(formatString + "\n");
    }
    try {
      OutputStream os = fs.create(machineListPath);
      IOUtils.write(builder.toString(), os);
      os.close();
      genFileToPath.put(machineListFilename, hdfsAppDir + "/gen/" + machineListFilename);
      LOG.info("Generate machine list of " + containerType + "'s containers successfully");
    } catch (IOException exp) {
    }
  }

  /**
   * setup container request, according to the following parameters
   * @param memory
   * @param vCores
   * @param host
   * @param requestPriority
   * @return container request
   */
  private static AMRMClient.ContainerRequest setupContainerRequest(
      int memory,
      int vCores,
      @NotNull String host,
      int requestPriority) {
    Resource capability = Resource.newInstance(memory, vCores);
    String[] nodes = new String[]{host};
    Priority priority = Priority.newInstance(requestPriority);
    boolean relaxLocality = false;
    return new AMRMClient.ContainerRequest(capability, nodes, null, priority, relaxLocality);
  }

  /**
   * setup genEnvToValue
   * @param containerType
   * @param containerPlacement
   * @return
   */
  private Map<String, String> setupGenEnvToValue(
      String containerType,
      ContainerPlacementStrategy.ContainerPlacement containerPlacement) {
    Map<String, String> genEnvToValue = new HashMap<String, String>();
    for (int i = 0; i != aMConf.getNumPorts(); ++i) {
      String name = AMConstants.GEN_ENV_PORT_ARG + "_" + i;
      genEnvToValue.put(name, System.getenv(name));
    }
    String host = containerPlacement.getHost();
    genEnvToValue.put(AMConstants.GEN_ENV_HOST, host);
    try {
      InetAddress address = InetAddress.getByName(host);
      String ip = address.getHostAddress();
      genEnvToValue.put(AMConstants.GEN_ENV_IP, ip);
    } catch (Exception exp) {
      LOG.error("Fail to resolve host " + host);
    }
    String numProcesses = containerPlacement.getNumProcesses().toString();
    genEnvToValue.put(AMConstants.GEN_ENV_NUM_PROCESSES, numProcesses);
    Integer rankCounter = typeToRankCounterMap.get(containerType);
    genEnvToValue.put(AMConstants.GEN_ENV_RANK, rankCounter.toString());
    typeToRankCounterMap.put(containerType, rankCounter + 1);
    return genEnvToValue;
  }

  public enum State {
    UNSTARTED, ALLOCATING, EXECUTING, CLEARING, EXITED
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public FinalApplicationResult getFinalApplicationResult() {
    return finalApplicationResult;
  }

  public Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> getTypeToCurrentPlacementsMap() {
    return typeToCurrentPlacementsMap;
  }

  public Map<String, Set<ContainerPlacementStrategy.ContainerPlacement>> getTypeToPendingPlacementsMap() {
    return typeToPendingPlacementsMap;
  }

  public Map<String, Integer> getTypeToRankCounterMap() {
    return typeToRankCounterMap;
  }

  public Map<AMRMClient.ContainerRequest, String> getRequestToTypeMap() {
    return requestToTypeMap;
  }

  public Map<AMRMClient.ContainerRequest, ContainerPlacementStrategy.ContainerPlacement> getRequestToPlacementMap() {
    return requestToPlacementMap;
  }

  public Map<ContainerId, String> getContainerIdToTypeMap() {
    return containerIdToTypeMap;
  }

  public Map<ContainerId, ContainerPlacementStrategy.ContainerPlacement> getContainerIdToPlacementMap() {
    return containerIdToPlacementMap;
  }

  public Map<ContainerPlacementStrategy.ContainerPlacement, Map<String, String>> getPlacementToGenMap() {
    return placementToGenMap;
  }

  public Map<String, AtomicInteger> getNumRunningProcessesMap() {
    return numRunningProcessesMap;
  }

  public Map<String, AtomicInteger> getNumRequestingProcessesMap() {
    return numRequestingProcessesMap;
  }

  public int getLaunchIndex() {
    return launchIndex;
  }

  public Map<String, Integer> getNumRequiredProcessesMap() {
    return numRequiredProcessesMap;
  }
}
