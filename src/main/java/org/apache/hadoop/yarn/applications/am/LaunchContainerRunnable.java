package org.apache.hadoop.yarn.applications.am;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.util.Shell;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.api.records.Container;
import org.apache.hadoop.yarn.api.records.ContainerLaunchContext;
import org.apache.hadoop.yarn.api.records.LocalResource;
import org.apache.hadoop.yarn.applications.conf.ContainerConf;
import org.apache.hadoop.yarn.applications.constants.AMConstants;
import org.apache.hadoop.yarn.applications.util.LocalResourceUtils;
import org.apache.hadoop.yarn.client.api.NMClient;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.util.Records;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by v-yihhe on 12/21/2015.
 */
public class LaunchContainerRunnable implements Runnable {

  private static final Log LOG = LogFactory.getLog(LaunchContainerRunnable.class);

  private AtomicInteger numLaunchingProcesses;

  private AtomicInteger numRunningProcesses;

  private String jarPath;

  private Integer protocolPort;

  private Boolean observeOutput;

  private Integer numProcesses;

  private Container container;

  private Configuration conf;

  private ContainerConf containerConf;

  private String containerType;

  private Map<String, String> genFileToPath;

  private Map<String, String> genEnvToValue;

  private NMClient nmClient;

  private ByteBuffer allTokens;

  // For test

  private boolean launchContainers;

  public LaunchContainerRunnable(
      AtomicInteger numLaunchingProcesses,
      AtomicInteger numRunningProcesses,
      String jarPath,
      Integer protocolPort,
      Boolean observeOutput,
      Integer numProcesses,
      Container container,
      Configuration conf,
      String containerType,
      ContainerConf containerConf,
      Map<String, String> genFileToPath,
      Map<String, String> genEnvToValue,
      ByteBuffer allTokens) {
    this.numLaunchingProcesses = numLaunchingProcesses;
    this.numRunningProcesses = numRunningProcesses;
    this.jarPath = jarPath;
    this.protocolPort = protocolPort;
    this.observeOutput = observeOutput;
    this.numProcesses = numProcesses;
    this.container = container;
    this.conf = conf;
    this.containerType = containerType;
    this.containerConf = containerConf;
    this.genFileToPath = genFileToPath;
    this.genEnvToValue = genEnvToValue;
    this.nmClient = null;
    this.allTokens = allTokens;
    String launchContainers = conf.get("popcorn.yarn.launchContainers");
    this.launchContainers = launchContainers == null ? true : Boolean.parseBoolean(launchContainers);
    numLaunchingProcesses.addAndGet(numProcesses);
  }

  @Override
  public void run() {
    LOG.info(String.format("Starting container, containerId = %s", container.getId().toString()));
    if (launchContainers) {
      nmClient = NMClient.createNMClient();
      nmClient.init(conf);
      nmClient.start();
    }
    startContainer();
  }

  private void startContainer() {
    LOG.info("Setting up ContainerLaunchContext");
    ContainerLaunchContext ctx = Records.newRecord(ContainerLaunchContext.class);
    Map<String, LocalResource> localResources = prepareLocalResources();
    ctx.setLocalResources(localResources);

    Map<String, String> environment = prepareEnvironment();
    ctx.setEnvironment(environment);

    List<String> commands = prepareCommand();
    ctx.setCommands(commands);

    if (launchContainers) {
      ctx.setTokens(allTokens.duplicate());
    }

    try {
      if (launchContainers) {
        nmClient.startContainer(container, ctx);
      }
      LOG.info(String.format("" +
              "Container %s starts successfully, numLaunchingProcesses = %d, numRunningProcesses = %d",
          container.getId().toString(),
          numLaunchingProcesses.addAndGet(-numProcesses),
          numRunningProcesses.addAndGet(numProcesses)));
    } catch (Exception exp) {
      LOG.error(String.format(
          "Fail to start container %s, due to exception %s",
          container.toString(),
          exp.getMessage()));
    }
  }

  private Map<String, String> prepareEnvironment() {
    LOG.info("Prepareing environment variables");
    Map<String, String> environment = new HashMap<String, String>();
    // Generated environment variables will be overwritten by envVars if env name in envVars is equal to env name
    // in generated env name.
    for (Map.Entry<String, String> entry : genEnvToValue.entrySet()) {
      String genEnv = entry.getKey();
      String value = genEnvToValue.get(genEnv);
      if (value == null) {
        LOG.error(String.format(
            "Required environment variable %s has not been generated", genEnv));
      } else {
        environment.put(genEnv, genEnvToValue.get(genEnv));
      }
    }
    for (Map.Entry<String, String> entry : containerConf.getEnvVars().entrySet()) {
        String key = entry.getKey();
        String value = entry.getValue();
        if (environment.containsKey(key)) {
            LOG.warn(String.format(
                    "Generated Environment variable %s is overwritten by envVars",
                    key));
        }
        environment.put(key, value);
    }
    StringBuilder classPathEnv = new StringBuilder(ApplicationConstants.Environment.CLASSPATH.$$())
        .append(ApplicationConstants.CLASS_PATH_SEPARATOR).append("./*");
    for (String c : conf.getStrings(
        YarnConfiguration.YARN_APPLICATION_CLASSPATH,
        YarnConfiguration.DEFAULT_YARN_CROSS_PLATFORM_APPLICATION_CLASSPATH)) {
      classPathEnv.append(ApplicationConstants.CLASS_PATH_SEPARATOR);
      classPathEnv.append(c.trim());
    }
    classPathEnv.append(ApplicationConstants.CLASS_PATH_SEPARATOR).append(
        "./log4j.properties");

    // add the runtime classpath needed for tests to work
    if (conf.getBoolean(YarnConfiguration.IS_MINI_YARN_CLUSTER, false)) {
      classPathEnv.append(':');
      classPathEnv.append(System.getProperty("java.class.path"));
    }
    environment.put("CLASSPATH", classPathEnv.toString());

    for (Map.Entry<String, String> entry : environment.entrySet()) {
      LOG.info("Set environment variable " + entry.getKey() + " = \"" + entry.getValue() + "\"");
    }
    return environment;
  }

  private Map<String, LocalResource> prepareLocalResources() {
    LOG.info("Preparing local resources");
    Map<String, LocalResource> localResources = new HashMap<String, LocalResource>();
    try {
      FileSystem fs = FileSystem.get(conf);
      // Add resource paths in container configuration to local resources
      // It has been guarentee that all paths are on hdfs
      for (String resourcePath : containerConf.getResourcePaths()) {
        Path path = new Path(resourcePath);
        LocalResourceUtils.addHdfsFiles(
            fs,
            path,
            "",
            localResources
       );
      }

      String executeDir = containerConf.getExecuteDir();

      // Add generated file paths necessary for container to local resources
      for (Map.Entry<String, String> entry : genFileToPath.entrySet()) {
        String genFile = entry.getKey();
        String pathStr = genFileToPath.get(genFile);
        if (pathStr == null) {
          LOG.error(String.format(
              "Required file %s has not been generated", genFile));
        } else {
          Path path = new Path(genFileToPath.get(genFile));
          LocalResourceUtils.addHdfsFiles(
              fs,
              path,
              executeDir,
              localResources
          );
        }
      }

      if (observeOutput) {
        Path path = new Path(jarPath);
        LocalResourceUtils.addHdfsFiles(
            fs,
            path,
            executeDir,
            localResources
        );
      }
    } catch (IOException exp) {
      LOG.error(String.format(
          "Fail to prepare local resources, due to exception %s",
          exp.getMessage()));
    }
    return localResources;
  }

  private List<String> prepareCommand() {
    LOG.info("Preparing command");
    String executeDir = containerConf.getExecuteDir();
    String executeFilename = containerConf.getExecuteFilename();
    StringBuilder commandBuilder = new StringBuilder();
    commandBuilder.append("cd " + executeDir + "\n");

    if (observeOutput) {
      commandBuilder.append(ApplicationConstants.Environment.JAVA_HOME.$$() + "/bin/java");
      commandBuilder.append(" " + ContainerStarter.class.getName());
      commandBuilder.append(" -e " + executeFilename);
      commandBuilder.append(" -p " + protocolPort);
      commandBuilder.append(" -container_type " + containerType);
      try {
        String hostname = InetAddress.getLocalHost().getHostName();
        commandBuilder.append(" -h " + hostname);
      } catch (Exception exp) {
      }
      commandBuilder.append(" -container_id " + container.getId().toString());
      commandBuilder.append(" 1>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/" + "AppMaster.stdout");
      commandBuilder.append(" 2>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/" + "AppMaster.stderr");
    } else {
      String executeCommand = executeFilename;
      if (!Shell.WINDOWS) {
        executeCommand = "./" + executeCommand;
      }
      executeCommand += " 1>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/stdout";
      executeCommand += " 2>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR + "/stderr";
      commandBuilder.append(executeCommand);
    }

    List<String> commands = new ArrayList<String>();
    commands.add(commandBuilder.toString());
    for (String command : commands) {
      LOG.info(String.format("Execute commands: %s", command));
    }
    return commands;
  }
}
