package org.apache.hadoop.yarn.applications.am;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.yarn.api.records.NodeReport;
import org.apache.hadoop.yarn.api.records.NodeState;
import org.apache.hadoop.yarn.api.records.Resource;
import org.apache.hadoop.yarn.client.api.YarnClient;

import java.util.*;
import java.util.List;

/**
 * Created by v-yihhe on 12/21/2015.
 */
public class ContainerPlacementStrategy {

  private static final Log LOG = LogFactory.getLog(ContainerPlacementStrategy.class);

  private Configuration conf;

  private YarnClient yarnClient;

  private int minimumAllocationMB;

  private int maximumAllocationMB;

  private int minimumAllocationVCores;

  private int maximumAllocationVCores;

  private Map<String, Set<String>> exclusionGraph;

  public ContainerPlacementStrategy(
      Configuration conf,
      YarnClient yarnClient,
      List<List<String>> exclusions) {
    this.conf = conf;
    this.yarnClient = yarnClient;
    this.minimumAllocationMB = Integer.parseInt(conf.get("yarn.scheduler.minimum-allocation-mb"));
    this.maximumAllocationMB = Integer.parseInt(conf.get("yarn.scheduler.maximum-allocation-mb"));
    this.minimumAllocationVCores = Integer.parseInt(conf.get("yarn.scheduler.minimum-allocation-vcores"));
    this.maximumAllocationVCores = Integer.parseInt(conf.get("yarn.scheduler.maximum-allocation-vcores"));
    this.exclusionGraph = new HashMap<String, Set<String>>();
    for (List<String> exclusion : exclusions) {
      String u = exclusion.get(0);
      String v = exclusion.get(1);
      Set<String> uAdjs = exclusionGraph.get(u);
      if (uAdjs == null) {
        uAdjs = new HashSet<String>();
        exclusionGraph.put(u, uAdjs);
      }
      uAdjs.add(v);
      Set<String> vAdjs = exclusionGraph.get(v);
      if (vAdjs == null) {
        vAdjs = new HashSet<String>();
        exclusionGraph.put(v, vAdjs);
      }
      vAdjs.add(u);
    }
    LOG.info(String.format(
        "Initializing ContainerPlacementStrategy: " +
            "minimumAllocationMB = %d, maximumAllocationMB = %d, " +
            "minimumAllocationVCores = %d, maximumAllocationVCores = %d",
        this.minimumAllocationMB,
        this.maximumAllocationMB,
        this.minimumAllocationVCores,
        this.maximumAllocationVCores));
  }

  /**
   * This method will be invoked when there is no requesting or launching process. So it only need to concern
   * typeToPendingPlacementsMap and typeToCurrentPlacementsMap. When dealing with typeToRequestMap, it just try to
   * satisfy each type of container placement requests one by one, with the consideration of existed placements,
   * according to exclusionGraph.
   * @param typeToPendingPlacementMap
   * @param typeToCurrentPlacementsMap
   * @param typeToRequestMap
   * @return newPlacementsMap
   */
  public Map<String, Set<ContainerPlacement>> generatePlacements(
      Map<String, Set<ContainerPlacement>> typeToPendingPlacementMap,
      Map<String, Set<ContainerPlacement>> typeToCurrentPlacementsMap,
      Map<String, ContainerPlacementRequest> typeToRequestMap) {
    List<NodeReport> nodeReports;
    try {
      nodeReports = yarnClient.getNodeReports(NodeState.RUNNING);
    } catch (Exception exp) {
      exp.printStackTrace();
      return null;
    }

    // Find out available resources according to node report
    Map<String, Resource> availableResources = new HashMap<String, Resource>();

    for (NodeReport nodeReport : nodeReports) {
      Resource capability = nodeReport.getCapability();
      Resource usedCapability = nodeReport.getUsed();
      String host = nodeReport.getNodeId().getHost();
      int numAvailableMemory = capability.getMemory() - usedCapability.getMemory();
      int numAvailableVCores = capability.getVirtualCores() - usedCapability.getVirtualCores();
      Resource availableResource = Resource.newInstance(numAvailableMemory, numAvailableVCores);
      availableResources.put(host, availableResource);
    }

    // Find out container types on each node, according to typeToNewPlacementsMap and typeToCurrentPlacementsMap
    Map<String, Set<String>> hostToTypesMap = new HashMap<String, Set<String>>();
    updateHostToTypesMap(hostToTypesMap, typeToPendingPlacementMap);
    updateHostToTypesMap(hostToTypesMap, typeToCurrentPlacementsMap);

    // Figure out placements for each type of container
    Map<String, Set<ContainerPlacement>> newPlacementsMap = new HashMap<String, Set<ContainerPlacement>>();
    for (Map.Entry<String, ContainerPlacementRequest> containerRequestEntry : typeToRequestMap.entrySet()) {
      String containerType = containerRequestEntry.getKey();
      ContainerPlacementRequest containerRequest = containerRequestEntry.getValue();
      int numRequestProcesses = containerRequest.getNumRequestProcesses();
      int processMemory = containerRequest.getNeedResource().getMemory();
      int processVCores = containerRequest.getNeedResource().getVirtualCores();
      boolean multipleProcessesPerContainer = containerRequest.isMultipleProcessesPerContainer();
      boolean multipleContainersPerNode =
          !exclusionGraph.containsKey(containerType) || !exclusionGraph.get(containerType).contains(containerType);
      for (Map.Entry<String, Resource> availableResourceEntry : availableResources.entrySet()) {
        String host = availableResourceEntry.getKey();
        boolean canBePlaced = true;
        if (hostToTypesMap.containsKey(host)) {
          for (String existedContainerType : hostToTypesMap.get(host)) {
            if (exclusionGraph.containsKey(containerType) && exclusionGraph.get(containerType).contains(existedContainerType)) {
              canBePlaced = false;
            }
          }
        }
        if (!canBePlaced) {
          continue;
        }
        Resource resource = availableResourceEntry.getValue();
        int numAvailableMemory = resource.getMemory();
        int numAvailableVCores = resource.getVirtualCores();
        int numAvailableProcessses = Math.min(
            numAvailableMemory / processMemory,
            numAvailableVCores / processVCores);
        int minNumProcessesInContainer = Math.min(
            (minimumAllocationMB + processMemory - 1) / processMemory,
            (maximumAllocationVCores + processVCores - 1) / processVCores);
        int maxNumProcessesInContainer = Math.min(
            maximumAllocationMB / processMemory,
            maximumAllocationVCores / processVCores);
        if (!multipleProcessesPerContainer) {
          minNumProcessesInContainer = maxNumProcessesInContainer = 1;
        }
        int numPlaceProcesses = Math.min(numAvailableProcessses, numRequestProcesses);
        if (numPlaceProcesses % maxNumProcessesInContainer != 0
            && numPlaceProcesses / maxNumProcessesInContainer * maxNumProcessesInContainer + minNumProcessesInContainer > numAvailableProcessses) {
          numPlaceProcesses = numPlaceProcesses / maxNumProcessesInContainer * maxNumProcessesInContainer;
        }
        if (!multipleContainersPerNode) {
          numPlaceProcesses = Math.min(numPlaceProcesses, maxNumProcessesInContainer);
        }
        resource.setMemory(numAvailableMemory - numPlaceProcesses * processMemory);
        resource.setVirtualCores(numAvailableVCores - numPlaceProcesses * processVCores);
        numRequestProcesses -= numPlaceProcesses;
        while (numPlaceProcesses > 0) {
          int numProcessesInContainer = Math.min(numPlaceProcesses, maxNumProcessesInContainer);
          Set<ContainerPlacement> containerPlacementSet = newPlacementsMap.get(containerType);
          if (containerPlacementSet == null) {
            containerPlacementSet = new HashSet<ContainerPlacement>();
            newPlacementsMap.put(containerType, containerPlacementSet);
          }
          ContainerPlacement containerPlacement = new ContainerPlacement(numProcessesInContainer, host);
          containerPlacementSet.add(containerPlacement);
          numPlaceProcesses -= numProcessesInContainer;
        }
        if (numRequestProcesses == 0) {
          break;
        }
      }
      if (numRequestProcesses > 0) {
        return null;
      }
    }
    return newPlacementsMap;
  }

  /**
   * Put entries in typeToPlacementMap into hostToTypesMap
   * @param hostToTypesMap
   * @param typeToPlacementMap
   */
  private void updateHostToTypesMap(
      Map<String, Set<String>> hostToTypesMap,
      Map<String, Set<ContainerPlacement>> typeToPlacementMap) {
    for (Map.Entry<String, Set<ContainerPlacement>> entry : typeToPlacementMap.entrySet()) {
      String containerType = entry.getKey();
      Set<ContainerPlacement> placements = entry.getValue();
      for (ContainerPlacement placement : placements) {
        Set<String> containerTypeSet = hostToTypesMap.get(placement.getHost());
        if (containerTypeSet == null) {
          containerTypeSet = new HashSet<String>();
          hostToTypesMap.put(placement.getHost(), containerTypeSet);
        }
        containerTypeSet.add(containerType);
      }
    }
  }

  public static class ContainerPlacementRequest {

    private int numRequestProcesses;

    private boolean multipleProcessesPerContainer;

    private Resource needResource;

    public ContainerPlacementRequest(
        int numRequestProcesses,
        boolean multipleProcessesPerContainer,
        Resource needResource) {
      this.numRequestProcesses = numRequestProcesses;
      this.multipleProcessesPerContainer = multipleProcessesPerContainer;
      this.needResource = needResource;
    }

    public int getNumRequestProcesses() {
      return numRequestProcesses;
    }

    public boolean isMultipleProcessesPerContainer() {
      return multipleProcessesPerContainer;
    }

    public Resource getNeedResource() {
      return needResource;
    }
  }

  public static class ContainerPlacement {

    private Integer numProcesses;

    private String host;

    public ContainerPlacement(Integer numProcesses, String host) {
      this.numProcesses = numProcesses;
      this.host = host;
    }

    public Integer getNumProcesses() {
      return numProcesses;
    }

    public String getHost() {
      return host;
    }
  }

}
