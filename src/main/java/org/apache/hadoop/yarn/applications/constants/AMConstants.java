package org.apache.hadoop.yarn.applications.constants;

/**
 * Created by v-yihhe on 12/14/2015.
 */
public class AMConstants {

  public static final int MIN_AVAILABLE_PORT = 10000;

  public static final int MAX_AVAILABLE_PORT = 20000;

  public static final int MINIMUM_WAIT_PERIOD = 2 * 60;

  public static final int MAXIMUM_WAIT_PERIOD = 2 * 60 * 60;

  // Environment variables passed from client to application master

  public static final String ENV_HDFS_APP_DIR = "HDFS_APP_DIR";

  public static final String ENV_SYNC_HOST = "SYNC_HOST";

  public static final String ENV_SYNC_PORT = "SYNC_PORT";

  public static final String ENV_RECEIVER_PORT = "RECEIVER_PORT";

  public static final String ENV_PORT_ARG = "PORT_ARG";

  public static final String ENV_ALLOCTIME = "ALLOCTIME";

  public static final String ENV_EXECTIME = "EXECTIME";

  public static final String GEN_ENV_HOST = "HOST";

  public static final String GEN_ENV_IP = "IP";

  public static final String GEN_ENV_RANK = "RANK";

  public static final String GEN_ENV_NUM_PROCESSES = "NUM_PROCESSES";

  public static final String GEN_ENV_PORT_ARG = "PORT_ARG";

  public static final String CONFIG_FILE = "config.json";
}
