/**
 * Description: Definition of AMConf class which parses config file
 * Created by v-yihhe on 12/14/2015.
 */

package org.apache.hadoop.yarn.applications.conf;

import com.google.gson.Gson;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.yarn.applications.util.ArgumentsParser;

import java.util.*;

public class AMConf {
    private Integer numPorts = 0;

    private String jarPath;

    private Map<String, ContainerConf> containerConfs = new HashMap<String, ContainerConf>();

    private List<String> launchOrder = new ArrayList<String>();

    private List<List<String>> exclusions = new ArrayList<List<String>>();

    public static AMConf parseArgsAndJson(String[] args, String json) throws ParseException {
        // Put arguments into options
        Gson gson = new Gson();
        Map<String, Object> confMap =
                gson.fromJson(json, HashMap.class);
        Map<String, Object> argumentsMap =
                (Map<String, Object>) confMap.get("arguments");
        if (argumentsMap != null && !argumentsMap.isEmpty()) {
            Set<String> optionNameSet = new HashSet<String>();
            for (Object obj : argumentsMap.values()) {
                optionNameSet.add((String) obj);
            }
            Options opts = new Options();
            for (String optionName : optionNameSet) {
                opts.addOption(optionName, true, "");
            }

            CommandLine cliParser = new ArgumentsParser().parse(opts, args);
            for (Map.Entry<String, Object> entry : argumentsMap.entrySet()) {
                // Get option value and set the value in configuration
                String optionName = (String) entry.getValue();
                if (cliParser.hasOption(optionName)) {
                    String optionValue = cliParser.getOptionValue(optionName);
                    String path = entry.getKey();
                    String[] names = path.split("/");
                    Object obj = confMap;
                    for (int nameIdx = 0; nameIdx != names.length; ++nameIdx) {
                        String name = names[nameIdx];
                        if (obj instanceof Map) {
                            Map map = (Map) obj;
                            if (!map.containsKey(name)) {
                                throw new IllegalArgumentException(String.format("Error argument path %s", path));
                            }
                            if (nameIdx != names.length - 1) {
                                obj = map.get(name);
                            } else {
                                map.put(name, optionValue);
                            }
                        } else if (obj instanceof List) {
                            List list = (List) obj;
                            int listIdx = Integer.parseInt(name);
                            if (listIdx < 0 || listIdx >= list.size()) {
                                throw new IllegalArgumentException(String.format("Error argument path %s", path));
                            }
                            if (nameIdx != names.length - 1) {
                                obj = list.get(listIdx);
                            } else {
                                list.set(listIdx, optionValue);
                            }
                        } else {
                            throw new IllegalArgumentException(String.format("Error argument path %s", path));
                        }
                    }
                }
            }
        }
        AMConf aMConf = gson.fromJson(gson.toJson(confMap), AMConf.class);
        return aMConf;
    }

    public Integer getNumPorts() {
        return numPorts;
    }

    public void setNumPorts(Integer numPorts) {
        this.numPorts = numPorts;
    }

    public String getJarPath() {
        return jarPath;
    }

    public void setJarPath(String jarPath) {
        this.jarPath = jarPath;
    }

    public Map<String, ContainerConf> getContainerConfs() {
        return containerConfs;
    }

    public void setContainerConfs(Map<String, ContainerConf> containerConfs) {
        this.containerConfs = containerConfs;
    }

    public List<String> getLaunchOrder() {
        return launchOrder;
    }

    public void setLaunchOrder(List<String> launchOrder) {
        this.launchOrder = launchOrder;
    }

    public List<List<String>> getExclusions() {
        return exclusions;
    }

    public void setExclusions(List<List<String>> exclusions) {
        this.exclusions = exclusions;
    }
}
