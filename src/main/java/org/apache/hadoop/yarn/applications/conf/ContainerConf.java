package org.apache.hadoop.yarn.applications.conf;

import org.apache.hadoop.yarn.applications.constants.AMConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by v-yihhe on 12/14/2015.
 */
public class ContainerConf {

  private Integer numProcesses;

  private Integer processMemory;

  private Integer processVCores;

  private Boolean multipleProcessesPerContainer = false;

  private Boolean waitForCompletion = false;

  private Boolean observeOutput = false;

  private Boolean flexible = false;

  private Boolean recoverable = false;

  private String executeDir = "";

  private String executeFilename;

  private String machineListFormat = "%" + AMConstants.GEN_ENV_HOST + "% %" + AMConstants.GEN_ENV_NUM_PROCESSES + "%";

  private List<String> resourcePaths = new ArrayList<String>();

  private Map<String, String> envVars = new HashMap<String, String>();

  public Integer getNumProcesses() {
    return numProcesses;
  }

  public void setNumProcesses(Integer numProcesses) {
    this.numProcesses = numProcesses;
  }

  public Integer getProcessMemory() {
    return processMemory;
  }

  public void setProcessMemory(Integer processMemory) {
    this.processMemory = processMemory;
  }

  public Integer getProcessVCores() {
    return processVCores;
  }

  public void setProcessVCores(Integer processVCores) {
    this.processVCores = processVCores;
  }

  public Boolean getMultipleProcessesPerContainer() {
    return multipleProcessesPerContainer;
  }

  public void setMultipleProcessesPerContainer(Boolean multipleProcessesPerContainer) {
    this.multipleProcessesPerContainer = multipleProcessesPerContainer;
  }

  public Boolean getWaitForCompletion() {
    return waitForCompletion;
  }

  public void setWaitForCompletion(Boolean waitForCompletion) {
    this.waitForCompletion = waitForCompletion;
  }

  public Boolean getObserveOutput() {
    return observeOutput;
  }

  public void setObserveOutput(Boolean observeOutput) {
    this.observeOutput = observeOutput;
  }

  public Boolean getFlexible() {
    return flexible;
  }

  public void setFlexible(Boolean flexible) {
    this.flexible = flexible;
  }

  public Boolean getRecoverable() {
    return recoverable;
  }

  public void setRecoverable(Boolean recoverable) {
    this.recoverable = recoverable;
  }

  public String getExecuteDir() {
    return executeDir;
  }

  public void setExecuteDir(String executeDir) {
    this.executeDir = executeDir;
  }

  public String getExecuteFilename() {
    return executeFilename;
  }

  public void setExecuteFilename(String executeFilename) {
    this.executeFilename = executeFilename;
  }

  public String getMachineListFormat() {
    return machineListFormat;
  }

  public void setMachineListFormat(String machineListFormat) {
    this.machineListFormat = machineListFormat;
  }

  public List<String> getResourcePaths() {
    return resourcePaths;
  }

  public void setResourcePaths(List<String> resourcePaths) {
    this.resourcePaths = resourcePaths;
  }

  public Map<String, String> getEnvVars() {
    return envVars;
  }

  public void setEnvVars(Map<String, String> envVars) {
    this.envVars = envVars;
  }
}
