package org.apache.hadoop.yarn.applications.util;

/**
 * Created by v-yihhe on 1/19/2016.
 */
public class NameUtils {
  public static String toFilename(String str) {
    StringBuilder builder = new StringBuilder();
    for (char ch : str.toCharArray()) {
      if (Character.isUpperCase(ch)) {
        builder.append("_" + Character.toLowerCase(ch));
      } else {
        builder.append(ch);
      }
    }
    return builder.toString();
  }

  public static String toEnvname(String str) {
    StringBuilder builder = new StringBuilder();
    for (char ch : str.toCharArray()) {
      if (Character.isUpperCase(ch)) {
        builder.append("_" + ch);
      } else {
        builder.append(Character.toUpperCase(ch));
      }
    }
    return builder.toString();
  }
}
