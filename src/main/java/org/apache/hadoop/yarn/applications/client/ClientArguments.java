package org.apache.hadoop.yarn.applications.client;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.yarn.applications.util.ArgumentsParser;
import org.apache.hadoop.yarn.applications.constants.ClientConstants;
import org.apache.log4j.Logger;

/**
 * Created by v-yihhe on 12/23/2015.
 */
public class ClientArguments {
    private static Options opts;
    private static Logger LOG = Logger.getLogger(ClientArguments.class);
    static {
        opts = new Options();
        opts.addOption(
                ClientConstants.OPT_HELP,
                false,
                "Print commandline options");
        opts.addOption(
                ClientConstants.OPT_IS_SYNC,
                false,
                "Enable synchronous mode, in which client will wait\n" +
                        "for app to finish, and stream AM stdout in real\n" +
                        "time");
        opts.addOption(
                ClientConstants.OPT_SYNC_PORT,
                true,
                "Port to be used in client for sync, when -s is enabled. "
                + "If sync port is not set, it will be assigned a random port.");
        opts.addOption(
                ClientConstants.OPT_RECEIVER_PORT,
                true,
                "Port to be used in am for receiving containers' output. "
                + "If receiver port is not set, it will be assigned a port.");
        opts.addOption(
                ClientConstants.OPT_ALLOCTIME,
                true,
                "Maximum allocation time. "
                + "If we cannot get enough container in this time, AM wil exit. Default 120s");
        opts.addOption(
                ClientConstants.OPT_EXECTIME,
                true,
                "Maximum execution time. "
                + "If program does not exit in this time, AM will exit. Default 7*24h");
        opts.addOption(ClientConstants.OPT_QUEUE,
                true,
                "Yarn queue to submit job to. Default to \"default\"");
        opts.addOption(ClientConstants.OPT_CONFIG,
                true,
                "The file path of configuration file, must be local file.");
        opts.addOption(ClientConstants.OPT_AM_PRIORITY,
                true,
                "The priority of application, default is 1.");
        opts.addOption(
                ClientConstants.OPT_APP_NAME,
                true,
                "Application name to appear in Yarn, default is \"PopCorn Application\".");
        opts.addOption(
                ClientConstants.OPT_AM_MEMORY,
                true,
                "Memory of application master, default is 1024MB."
        );
        opts.addOption(
                ClientConstants.OPT_AM_VCORES,
                true,
                "Virtual cores of application master, default is 4."
        );
        opts.addOption(
                ClientConstants.OPT_VERBOSE,
                false,
                "Whether to output verbose information, default false."
        );
    }
    /* should print help info */
    private Boolean printHelp;
    /* running in synchronous mode*/
    private Boolean isSync;
    /* which port to use in sync mode */
    private Integer syncPort;
    /* port to use for AM to collect container logs */
    private Integer receiverPort;
    /* config file path */
    private String configFile;
    /* name of the app */
    private String appName;
    /* queue to submit to */
    private String amQueue;
    /* priority of the am */
    private Integer amPriority;
    /* number of v cores for am */
    private Integer amVCores;
    /* memory in MB for am */
    private Integer amMemory;
    /* allocation timeout */
    private Integer allocationTimeout;
    /* execution timeout */
    private Integer executionTimeout;
    /* should we print verbose info? */
    private boolean verboseOn;

    public static ClientArguments parseArgs(String[] args) throws ParseException {
        CommandLine cliParser = new ArgumentsParser().parse(opts, args);
        ClientArguments clientArguments = new ClientArguments();
        clientArguments.printHelp =
                cliParser.hasOption(ClientConstants.OPT_HELP);
        if (clientArguments.printHelp) {
            return clientArguments;
        }
        clientArguments.isSync =
                cliParser.hasOption(ClientConstants.OPT_IS_SYNC);
        if (cliParser.hasOption(ClientConstants.OPT_SYNC_PORT)) {
            Integer syncPort =
                    Integer.valueOf(cliParser.getOptionValue(ClientConstants.OPT_SYNC_PORT));
            if (syncPort < 0 || syncPort >= 65536) {
                throw new ParseException("illegal sync port value: " + syncPort);
            }
            clientArguments.syncPort = syncPort;
        } else {
            clientArguments.syncPort = null;
        }
        if (cliParser.hasOption(ClientConstants.OPT_RECEIVER_PORT)) {
            Integer recvPort =
                    Integer.valueOf(cliParser.getOptionValue(ClientConstants.OPT_RECEIVER_PORT));
            if (recvPort < 0 || recvPort >= 65536) {
                throw new ParseException("illegal receiver port value: " + recvPort);
            }
            clientArguments.receiverPort = recvPort;
        } else {
            clientArguments.receiverPort = null;
        }
        clientArguments.amQueue =
                cliParser.getOptionValue(ClientConstants.OPT_QUEUE, "default");
        clientArguments.appName =
                cliParser.getOptionValue(ClientConstants.OPT_APP_NAME, "PopCorn Application");
        clientArguments.amPriority =
                Integer.valueOf(cliParser.getOptionValue(ClientConstants.OPT_AM_PRIORITY, "0"));
        clientArguments.allocationTimeout =
                Integer.valueOf(cliParser.getOptionValue(
                        ClientConstants.OPT_ALLOCTIME,
                        "" + ClientConstants.DEFAULT_ALLOCTIME));
        if (clientArguments.allocationTimeout < 0) {
            throw new ParseException("illegal allocation timeout value: "
                    + clientArguments.allocationTimeout);
        }
        clientArguments.executionTimeout =
                Integer.valueOf(cliParser.getOptionValue(
                        ClientConstants.OPT_EXECTIME,
                        "" + ClientConstants.DEFAULT_EXECTIME));
        if (clientArguments.executionTimeout < 0) {
            throw new ParseException("illegal execution timeout value: "
                    + clientArguments.executionTimeout);
        }
        clientArguments.amMemory =
                Integer.valueOf(cliParser.getOptionValue(
                        ClientConstants.OPT_AM_MEMORY,
                        "" + ClientConstants.DEFAULT_AM_MEMORY));
        if (clientArguments.amMemory < 0) {
            throw new ParseException("illegal AM memory value: "
                    + clientArguments.amMemory);
        }
        clientArguments.amVCores =
                Integer.valueOf(cliParser.getOptionValue(
                    ClientConstants.OPT_AM_VCORES,
                    "" + ClientConstants.DEFAULT_AM_VCORES));
        if (clientArguments.amVCores < 0) {
            throw new ParseException("illegal AM vcore value: "
                    + clientArguments.amVCores);
        }
        clientArguments.verboseOn =
                cliParser.hasOption(ClientConstants.OPT_VERBOSE);
        if (!cliParser.hasOption(ClientConstants.OPT_CONFIG)) {
            throw new ParseException("config file path not specified");
        }
        else {
            clientArguments.configFile =
                    cliParser.getOptionValue(ClientConstants.OPT_CONFIG);
        }
        return clientArguments;
    }
    public static void printUsage() {
        new HelpFormatter().printHelp("Client", opts);
    }
    public Boolean getPrintHelp() {
        return printHelp;
    }
    public Boolean getIsSync() {
        return isSync;
    }
    public Integer getSyncPort() {
        return syncPort;
    }
    public Integer getReceiverPort() {
        return receiverPort;
    }
    public String getConfigFile() {
        return configFile;
    }
    public String getAppName() {
        return appName;
    }
    public String getAmQueue() {
        return amQueue;
    }
    public Integer getAmPriority() {
        return amPriority;
    }
    public Integer getAmVCores() {
        return amVCores;
    }
    public Integer getAmMemory() {
        return amMemory;
    }
    public Integer getAllocationTimeout() {
        return allocationTimeout;
    }
    public Integer getExecutionTimeout() {
        return executionTimeout;
    }
    public boolean getVerbose() {
        return verboseOn;
    }
}
