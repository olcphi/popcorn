package org.apache.hadoop.yarn.applications.port.impl;

import org.apache.hadoop.yarn.applications.port.PortManager;
import org.apache.hadoop.yarn.applications.constants.AMConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by v-yihhe on 1/6/2016.
 */
public class RandomPortManagerImpl extends PortManager {

  private List<Integer> ports;

  private Integer index;

  public RandomPortManagerImpl() {
    ports = new ArrayList<Integer>();
    for (int portNumber = AMConstants.MIN_AVAILABLE_PORT; portNumber <= AMConstants.MAX_AVAILABLE_PORT; ++portNumber) {
      ports.add(portNumber);
    }

    Random random = new Random(System.currentTimeMillis());
    for (int idx = 0; idx != ports.size(); ++idx) {
      int swapIdx = idx + random.nextInt(ports.size() - idx);
      int temp = ports.get(idx);
      ports.set(idx, ports.get(swapIdx));
      ports.set(swapIdx, temp);
    }

    index = 0;
  }

  @Override
  public List<Integer> requestPorts(int numPorts) {
    if (numPorts > ports.size()) {
      throw new IllegalArgumentException(
          String.format(
              "numPorts = %d is larger than the number of available ports = %d",
              numPorts,
              ports.size()));
    }
    List<Integer> result = new ArrayList<Integer>();
    for (; numPorts > 0; --numPorts) {
      result.add(ports.get(index));
      ++index;
      if (index == ports.size()) {
        index = 0;
      }
    }
    return result;
  }
}
