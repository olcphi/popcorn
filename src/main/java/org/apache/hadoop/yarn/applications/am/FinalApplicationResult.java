package org.apache.hadoop.yarn.applications.am;

import org.apache.hadoop.yarn.api.records.FinalApplicationStatus;

/**
 * Created by v-yihhe on 12/8/2015.
 */
public class FinalApplicationResult {

  private FinalApplicationStatus finalApplicationStatus;

  private String appMessage;

  private String trackingUrl;

  public FinalApplicationResult() {
    finalApplicationStatus = FinalApplicationStatus.UNDEFINED;
    appMessage = "unkown result";
    trackingUrl = null;
  }

  public FinalApplicationStatus getFinalApplicationStatus() {
    return finalApplicationStatus;
  }

  public void setFinalApplicationStatus(FinalApplicationStatus finalApplicationStatus) {
    this.finalApplicationStatus = finalApplicationStatus;
  }

  public String getAppMessage() {
    return appMessage;
  }

  public void setAppMessage(String appMessage) {
    this.appMessage = appMessage;
  }

  public String getTrackingUrl() {
    return trackingUrl;
  }

  public void setTrackingUrl(String trackingUrl) {
    this.trackingUrl = trackingUrl;
  }
}
