package org.apache.hadoop.yarn.applications.util;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by v-yihhe on 1/19/2016.
 */
public class PathUtils {
  public static String getRelativePath(String from, String to) {
    Path pathFrom = Paths.get(from);
    Path pathTo = Paths.get(to);
    Path pathRelative = pathFrom.relativize(pathTo);
    return pathRelative.toString();
  }
}
