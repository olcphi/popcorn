package org.apache.hadoop.yarn.applications.constants;

/**
 * Created by heyihong on 16/1/12.
 */
public class ClientConstants {
    public static final String OPT_HELP = "help";
    public static final String OPT_QUEUE = "queue";
    public static final String OPT_IS_SYNC = "sync";
    public static final String OPT_SYNC_PORT = "sync_port";
    public static final String OPT_RECEIVER_PORT = "receive_port";
    public static final String OPT_CONFIG = "config";
    public static final String OPT_ALLOCTIME = "alloctime";
    public static final String OPT_EXECTIME = "exectime";
    public static final String OPT_AM_PRIORITY = "priority";
    public static final String OPT_APP_NAME = "appname";
    public static final String OPT_AM_VCORES = "am_vcores";
    public static final String OPT_AM_MEMORY = "am_memory";
    public static final String OPT_VERBOSE = "verbose";
    public static final int DEFAULT_AM_MEMORY = 4096;
    public static final int DEFAULT_AM_VCORES = 4;
    public static final int DEFAULT_ALLOCTIME = 2 * 60;
    public static final int DEFAULT_EXECTIME = 7 * 24 * 60 * 60;

    public static final int EXIT_CODE_SUCCESS = 0;
    public static final int EXIT_CODE_PARSING_ERROR = 1;
    public static final int EXIT_CODE_PROGRM_FAIL = 2;
    public static final int EXIT_CODE_UNEXPECTED_ERROR = 3;
    public static final int EXIT_CODE_PRINT_HELP = 4;
    public static final int EXIT_CODE_IO_ERROR = 5;
}
