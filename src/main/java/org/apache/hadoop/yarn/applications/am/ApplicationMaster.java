/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.hadoop.yarn.applications.am;

import java.io.*;
import java.lang.System;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.classification.InterfaceAudience;
import org.apache.hadoop.classification.InterfaceStability;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.net.NetUtils;
import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.security.token.Token;
import org.apache.hadoop.util.ExitUtil;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.api.ApplicationConstants.Environment;
import org.apache.hadoop.yarn.api.protocolrecords.RegisterApplicationMasterResponse;
import org.apache.hadoop.yarn.api.records.ApplicationAttemptId;
import org.apache.hadoop.yarn.api.records.ContainerId;
import org.apache.hadoop.yarn.api.records.FinalApplicationStatus;
import org.apache.hadoop.yarn.applications.conf.AMConf;
import org.apache.hadoop.yarn.applications.constants.AMConstants;
import org.apache.hadoop.yarn.client.api.AMRMClient;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.hadoop.yarn.security.AMRMTokenIdentifier;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.net.SocketAppender;

@InterfaceAudience.Public
@InterfaceStability.Unstable
public class ApplicationMaster {
  private static final Log LOG = LogFactory.getLog(ApplicationMaster.class);

  // Configuration
  private Configuration conf;

  private AMConf aMConf;

  private String hdfsAppDir;

  private Integer receiverPort = null;

  private ExecutorService threadPool = Executors.newFixedThreadPool(10);

  private Integer allocationTimeout;

  private Integer executionTimeout;

  /**
   * @param args Command line args
   */
  public static void main(String[] args) {
    // Configuring log4j

    // Output log to client
    String syncHost = System.getenv(AMConstants.ENV_SYNC_HOST);
    if (syncHost != null) {
      Integer syncPort = Integer.valueOf(System.getenv(AMConstants.ENV_SYNC_PORT));
      SocketAppender socketAppender = new SocketAppender();
      socketAppender.setRemoteHost(syncHost);
      socketAppender.setPort(syncPort);
      socketAppender.setReconnectionDelay(1000);
      socketAppender.setThreshold(Level.INFO);
      socketAppender.activateOptions();
      Logger.getRootLogger().addAppender(socketAppender);
    }

    // Output log to stdout
    ConsoleAppender console = new ConsoleAppender();
    String PATTERN = " [AM] %d %p %m%n";
    console.setLayout(new PatternLayout(PATTERN));
    console.setThreshold(Level.INFO);
    console.setTarget("System.out");
    console.activateOptions();
    Logger.getRootLogger().addAppender(console);

    boolean result = false;
    try {
      ApplicationMaster appMaster = new ApplicationMaster();
      LOG.info("Initializing ApplicationMaster");
      appMaster.init(args);
      result = appMaster.run();
    } catch (Exception e) {
      e.printStackTrace();
      ExitUtil.terminate(1, e);
    }
    if (result) {
      LOG.info("Application Master completed successfully.");
      System.exit(0);
    } else {
      LOG.info("Application Master failed. exiting");
      System.exit(2);
    }
  }

  public ApplicationMaster() {
    conf = new YarnConfiguration();
  }

  /**
   * Parse command line options
   *
   * @param args Command line args
   * @return Whether init successful and run should be invoked
   * @throws ParseException
   * @throws IOException
   */
  public void init(String[] args) throws ParseException, IOException {
    Map<String, String> envs = System.getenv();

    if (!envs.containsKey(Environment.CONTAINER_ID.name())) {
      throw new IllegalArgumentException(
          "Application Attempt Id not set in the environment");
    } else {
      ContainerId containerId = ConverterUtils.toContainerId(envs
          .get(Environment.CONTAINER_ID.name()));
      ApplicationAttemptId appAttemptID = containerId.getApplicationAttemptId();

      LOG.info("Application master for app" + ", appId="
          + appAttemptID.getApplicationId().getId() + ", clustertimestamp="
          + appAttemptID.getApplicationId().getClusterTimestamp()
          + ", attemptId=" + appAttemptID.getAttemptId());
    }

    if (!envs.containsKey(ApplicationConstants.APP_SUBMIT_TIME_ENV)) {
      throw new RuntimeException(ApplicationConstants.APP_SUBMIT_TIME_ENV
          + " not set in the environment");
    }
    if (!envs.containsKey(Environment.NM_HOST.name())) {
      throw new RuntimeException(Environment.NM_HOST.name()
          + " not set in the environment");
    }
    if (!envs.containsKey(Environment.NM_HTTP_PORT.name())) {
      throw new RuntimeException(Environment.NM_HTTP_PORT
          + " not set in the environment");
    }
    if (!envs.containsKey(Environment.NM_PORT.name())) {
      throw new RuntimeException(Environment.NM_PORT.name()
          + " not set in the environment");
    }

    hdfsAppDir = envs.get(AMConstants.ENV_HDFS_APP_DIR);

    // Get aMConf
    String jsonConfig =
        IOUtils.toString(new FileInputStream(AMConstants.CONFIG_FILE), "UTF-8");
    aMConf = AMConf.parseArgsAndJson(new String[]{}, jsonConfig);

    allocationTimeout = Integer.valueOf(envs.get(AMConstants.ENV_ALLOCTIME));

    executionTimeout = Integer.valueOf(envs.get(AMConstants.ENV_EXECTIME));

    if (envs.get(AMConstants.ENV_RECEIVER_PORT) != null) {
      receiverPort = Integer.valueOf(envs.get(AMConstants.ENV_RECEIVER_PORT));
      threadPool.execute(new ContainerOutputReceiverRunnable(receiverPort));
    }
  }

  /**
   * Main run function for the application master
   *
   * @throws YarnException
   * @throws IOException
   */
  @SuppressWarnings({"unchecked"})
  public boolean run() throws YarnException, IOException {
    LOG.info("ApplicationMaster started");

    Credentials credentials = UserGroupInformation.getCurrentUser()
        .getCredentials();
    DataOutputBuffer dob = new DataOutputBuffer();
    credentials.writeTokenStorageToStream(dob);
    Iterator<Token<?>> iter = credentials.getAllTokens().iterator();
    LOG.info("Executing with tokens:");
    while (iter.hasNext()) {
      Token<?> token = iter.next();
      LOG.info(token);
      if (token.getKind().equals(AMRMTokenIdentifier.KIND_NAME)) {
        iter.remove();
      }
    }
    ByteBuffer allTokens = ByteBuffer.wrap(dob.getData(), 0, dob.getLength());
    // Create appSubmitterUgi and add original tokens to it
    String appSubmitterUserName = System
        .getenv(ApplicationConstants.Environment.USER.name());
    UserGroupInformation appSubmitterUgi =
        UserGroupInformation.createRemoteUser(appSubmitterUserName);
    appSubmitterUgi.addCredentials(credentials);

    // Initialize amrmClient
    LOG.info("Initializing amrmClient.");
    AMRMClient amrmClient = AMRMClient.createAMRMClient();
    amrmClient.init(conf);
    amrmClient.start();

    String appMasterHostname = NetUtils.getHostname();
    int appMasterRpcPort = -1;
    String appMasterTrackingUrl = "";
    try {
      RegisterApplicationMasterResponse response =
          amrmClient.registerApplicationMaster(appMasterHostname, appMasterRpcPort, appMasterTrackingUrl);
      LOG.info("Register application master successfully.");
    } catch (Exception exp) {
      LOG.error("Fail to register application master.");
    }

    // Initialize yarnClient
    LOG.info("Initializing yarnClient");
    YarnClient yarnClient = YarnClient.createYarnClient();
    yarnClient.init(conf);
    yarnClient.start();

    ContainerAllocator containerAllocator =
        new ContainerAllocator(
            conf,
            amrmClient,
            yarnClient,
            aMConf,
            hdfsAppDir,
            receiverPort,
            allTokens
        );

    LOG.info(String.format(
            "Start allocating containers, container allocator state = %s",
            containerAllocator.getState()));

    long totalAllocationTime = 0;
    long totalExecutionTime = 0;
    long currentTime = System.currentTimeMillis();
    Random random = new Random(currentTime);
    int waitPeriod = AMConstants.MINIMUM_WAIT_PERIOD;
    while (containerAllocator.getState() != ContainerAllocator.State.EXITED) {
      long previousTime = currentTime;
      currentTime = System.currentTimeMillis();
      switch (containerAllocator.getState()) {
        case ALLOCATING:
          totalAllocationTime += currentTime - previousTime;
          if (allocationTimeout != 0 && totalAllocationTime > allocationTimeout * 1000) {
            LOG.info(String.format(
                "Allocation time exceeded timeout %d second(s), stopping container allocator",
                allocationTimeout));
            containerAllocator.clear();
            int sleepTime = random.nextInt(waitPeriod);
            LOG.info(String.format(
                "Finish clear container allocator, sleep for %d second(s).",
                sleepTime));
            try {
              Thread.sleep(sleepTime * 1000);
            } catch (Exception exp) {
            }
            LOG.info("Restart allocating");
            waitPeriod = Math.min(waitPeriod * 2, AMConstants.MAXIMUM_WAIT_PERIOD);
            totalAllocationTime = 0;
          }
          break;
        case EXECUTING:
          totalExecutionTime += currentTime - previousTime;
          if (executionTimeout != 0 && totalExecutionTime > executionTimeout * 1000) {
            containerAllocator.finish(
                String.format(
                    "Application failed. Due to execution time exceeded %d second(s).",
                    executionTimeout),
                FinalApplicationStatus.FAILED,
                null
            );
          }
          break;
        default:
          // Do nothing
      }
      ContainerAllocator.State prevState = containerAllocator.getState();
      containerAllocator.allocateResources();
      ContainerAllocator.State currentState = containerAllocator.getState();
      if (prevState != currentState) {
        LOG.info(String.format(
                "Container allocator state transfers from %s to %s",
                prevState,
                currentState));
      }
      try {
        Thread.sleep(1000);
      } catch (Exception exp) {
      }
    }

    LOG.info("Shutdown thread pool.");
    threadPool.shutdown();

    LOG.info("Stopping yarnClient.");
    yarnClient.stop();

    FinalApplicationResult finalApplicationResult = containerAllocator.getFinalApplicationResult();

    LOG.info("Stopping amrmClient");
    try {
      amrmClient.unregisterApplicationMaster(
          finalApplicationResult.getFinalApplicationStatus(),
          finalApplicationResult.getAppMessage(),
          finalApplicationResult.getTrackingUrl());
      LOG.info("Unregister application master successfully.");
    } catch (Exception exp) {
      LOG.error("Fail to unregister application master");
    }
    LOG.info(String.format("App message = %s", finalApplicationResult.getAppMessage()));
    LOG.info(String.format("Final application status = %s", finalApplicationResult.getFinalApplicationStatus()));
    amrmClient.stop();

    return finalApplicationResult.getFinalApplicationStatus().equals(FinalApplicationStatus.SUCCEEDED);
  }

  private class ContainerOutputReceiverRunnable implements Runnable {

    private int port;

    public ContainerOutputReceiverRunnable(int port) {
      this.port = port;
    }

    @Override
    public void run() {
      try {
        ServerSocket server = new ServerSocket(port);
        while (true) {
          Socket socket = server.accept();
          LOG.info("Receive socket from " + socket.getInetAddress().getHostName());
          threadPool.execute(new ContainerOutputReceiverHandlerRunnable(socket));
        }
      } catch (IOException exp) {
        LOG.error("Server closed by exception: " + exp.getMessage());
    }
  }

  private class ContainerOutputReceiverHandlerRunnable implements Runnable {

    private Socket socket;

    public ContainerOutputReceiverHandlerRunnable(Socket socket) {
      this.socket = socket;
    }

    @Override
    public void run() {
      try {
        BufferedReader is = new BufferedReader(new InputStreamReader(
            socket.getInputStream()));
        String line;
        while ((line = is.readLine()) != null) {
          LOG.info(line);
        }
        is.close();
        socket.close();
      } catch (IOException exp) {
        LOG.error("Socket closed by exception: " + exp.getMessage());
      }
      }
    }
  }
}