package org.apache.hadoop.yarn.applications.port;

import org.apache.hadoop.yarn.applications.port.impl.RandomPortManagerImpl;

import java.util.List;

/**
 * Created by v-yihhe on 1/6/2016.
 */
public abstract class PortManager {

  public abstract List<Integer> requestPorts(int numPorts);

  public static PortManager getInstance() {
    if (portManager == null) {
      portManager = new RandomPortManagerImpl();
    }
    return portManager;
  }

  private static PortManager portManager = null;

}
