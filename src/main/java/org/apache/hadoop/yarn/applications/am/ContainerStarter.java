package org.apache.hadoop.yarn.applications.am;

import org.apache.hadoop.util.Shell;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by v-yihhe on 1/11/2016.
 */
public class ContainerStarter {

  private static String executeFile;

  private static int amPort;

  private static String amHost;

  private static String containerIdStr;

  private static AtomicInteger numWaitThreads;

  private static String containerType;

  private static boolean observeOutput = false;

  public static void init(String[] args) throws Exception {
    for (int idx = 0; idx != args.length; ++idx) {
      if ("-e".equals(args[idx])) {
        executeFile = args[++idx];
      } else if ("-p".equals(args[idx])) {
        amPort = Integer.parseInt(args[++idx]);
      } else if ("-h".equals(args[idx])) {
        amHost = args[++idx];
      } else if ("-container_id".equals(args[idx])) {
        containerIdStr = args[++idx];
      } else if ("-container_type".equals(args[idx])) {
        containerType = args[++idx];
      }
    }
  }

  public static void main(String[] args) throws Exception {
    init(args);
    File file = new File(executeFile);
    file.setExecutable(true);
    String executeCommand = executeFile;
    if (!Shell.WINDOWS) {
      executeCommand = "./" + executeCommand;
    }
    String[] commands = {executeCommand};
    Process process = Runtime.getRuntime().exec(commands);
    ExecutorService threadPool = Executors.newFixedThreadPool(2);
    numWaitThreads = new AtomicInteger(2);
    StringBuilder builder = new StringBuilder();
    threadPool.execute(new WatchOutputRunnable(OutputType.STDOUT, process.getInputStream(), builder));
    threadPool.execute(new WatchOutputRunnable(OutputType.STDERR, process.getErrorStream(), builder));
    while (numWaitThreads.get() > 0) {
      Thread.sleep(300);
      synchronized (builder) {
        if (builder.length() > 0) {
          Socket socket = new Socket(amHost, amPort);
          PrintWriter pw = new PrintWriter(socket.getOutputStream());
          pw.print(builder.toString());
          builder.setLength(0);
          pw.close();
          socket.close();
        }
      }
    }
    int exitValue = process.exitValue();
    threadPool.shutdown();
    System.exit(exitValue);
  }

  private static class WatchOutputRunnable implements Runnable {

    private OutputType type;

    private InputStream is;

    private StringBuilder builder;

    public WatchOutputRunnable(OutputType type, InputStream is, StringBuilder builder) {
      this.type = type;
      this.is = is;
      this.builder = builder;
    }

    @Override
    public void run() {
      BufferedReader br = new BufferedReader(new InputStreamReader(is));
      String msg;

      try {
        while ((msg = br.readLine()) != null) {
          if (type.equals(OutputType.STDOUT)) {
            synchronized (builder) {
              System.out.println(msg);
              builder.append(containerType + " " + containerIdStr + " " + type.getName() + ": " + msg + "\n");
            }
          } else if (type.equals(OutputType.STDERR)) {
            synchronized (builder) {
              System.err.println(msg);
              builder.append(containerType + " " + containerIdStr + " " + type.getName() + ": " + msg + "\n");
            }
          }
        }
        br.close();
      } catch (IOException exp) {
        System.err.println(exp.getMessage());
      }
      numWaitThreads.decrementAndGet();
    }
  }

  private enum OutputType {
    STDOUT("stdout"), STDERR("stderr");

    private String name;

    OutputType(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }
  }

}
