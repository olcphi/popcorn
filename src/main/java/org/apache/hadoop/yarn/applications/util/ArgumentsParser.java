package org.apache.hadoop.yarn.applications.util;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

import java.util.ListIterator;

/**
 * Created by v-yihhe on 1/7/2016.
 */
public class ArgumentsParser extends BasicParser {

  @Override
  protected void processOption(final String arg, final ListIterator iter) throws ParseException {
    boolean hasOption = getOptions().hasOption(arg);
    if (hasOption) {
      super.processOption(arg, iter);
    }
  }
}
