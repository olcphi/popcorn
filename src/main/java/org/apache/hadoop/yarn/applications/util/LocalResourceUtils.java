package org.apache.hadoop.yarn.applications.util;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.yarn.api.records.LocalResource;
import org.apache.hadoop.yarn.api.records.LocalResourceType;
import org.apache.hadoop.yarn.api.records.LocalResourceVisibility;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.util.Map;

/**
 * Created by v-yihhe on 11/19/2015.
 */
public class LocalResourceUtils {

  private static final Logger LOG = Logger.getLogger(LocalResourceUtils.class);

  /***
   * if resourcePath specifies the location of a file,
   * the function will add it in to localResources.
   * if resourcePath specifies the location of a directory,
   * the function will add all files inside it recursively.
   *
   * @param fs
   * @param resourcePath
   * @param resourceName
   * @param localResources
   * @throws IOException
   */
  public static void addHdfsFiles(
      FileSystem fs,
      Path resourcePath,
      String resourceName,
      Map<String, LocalResource> localResources) throws IOException {
    FileStatus resourceFileStatus = fs.getFileStatus(resourcePath);
    if (!resourceName.isEmpty()) {
      resourceName += "/";
    }
    resourceName += resourcePath.getName();
    if (resourceFileStatus.isDirectory()) {
      for (FileStatus fileStatus : fs.listStatus(resourcePath)) {
        Path subResourcePath = fileStatus.getPath();
        addHdfsFiles(
            fs,
            subResourcePath,
            resourceName,
            localResources
        );
      }
    } else if (resourceFileStatus.isFile()) {
      LocalResource localResource = LocalResource.newInstance(
          ConverterUtils.getYarnUrlFromURI(resourceFileStatus.getPath().toUri()),
          LocalResourceType.FILE,
          LocalResourceVisibility.APPLICATION,
          resourceFileStatus.getLen(),
          resourceFileStatus.getModificationTime());
      LOG.info(String.format(
          "add %s, resouceName = %s",
          localResource.toString(),
          resourceName));
      localResources.put(resourceName, localResource);
    }
  }
}
