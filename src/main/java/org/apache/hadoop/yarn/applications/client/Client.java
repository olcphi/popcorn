/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.hadoop.yarn.applications.client;

import com.google.gson.Gson;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.classification.InterfaceAudience;
import org.apache.hadoop.classification.InterfaceStability;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DataOutputBuffer;
import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.security.token.Token;
import org.apache.hadoop.yarn.api.ApplicationConstants;
import org.apache.hadoop.yarn.api.ApplicationConstants.Environment;
import org.apache.hadoop.yarn.api.records.*;
import org.apache.hadoop.yarn.api.records.Priority;
import org.apache.hadoop.yarn.applications.am.ApplicationMaster;
import org.apache.hadoop.yarn.applications.conf.AMConf;
import org.apache.hadoop.yarn.applications.conf.ContainerConf;
import org.apache.hadoop.yarn.applications.constants.AMConstants;
import org.apache.hadoop.yarn.applications.constants.ClientConstants;
import org.apache.hadoop.yarn.applications.port.PortManager;
import org.apache.hadoop.yarn.applications.util.LocalResourceUtils;
import org.apache.hadoop.yarn.applications.util.NameUtils;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.client.api.YarnClientApplication;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.hadoop.yarn.util.Records;
import org.apache.log4j.*;
import org.apache.log4j.spi.LoggingEvent;

import java.io.*;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@InterfaceAudience.Public
@InterfaceStability.Unstable
public class Client {
    private static Logger LOG = Logger.getLogger(Client.class);
    private Configuration conf;
    private FileSystem fs;
    private String amClassName;
    private YarnClient yarnClient;
    private AMConf aMConf;
    private ClientArguments clientArguments;
    private String hdfsAppDir;
    private String syncHost;
    private Integer syncPort = null;
    private Integer receiverPort = null;
    private List<Integer> portArgs;
    // flag to indicate whether to keep containers across application attempts.
    private static final boolean keepContainers = false;
    private ExecutorService threadPool = Executors.newFixedThreadPool(2);

    /**
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        try {
            Configuration yarnConf = new YarnConfiguration();
            String amClassName = ApplicationMaster.class.getName();
            Client client = new Client(amClassName, yarnConf);
            if (client.init(args) == ClientConstants.EXIT_CODE_SUCCESS) {
                int r = client.run();
                System.exit(r);
            }
            else {
                System.exit(ClientConstants.EXIT_CODE_PARSING_ERROR);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(ClientConstants.EXIT_CODE_UNEXPECTED_ERROR);
        }
    }

    private Client(String amClassName, Configuration conf) {
        this.conf = conf;
        this.amClassName = amClassName;
        yarnClient = YarnClient.createYarnClient();
        yarnClient.init(conf);
    }

    /**
     * Parse command line options and configuration file
     *
     * @param args Parsed command line options
     * @return Whether the init was successful to run the client
     * @throws ParseException
     */
    public int init(String[] args) {
        if (args.length == 0) {
            ClientArguments.printUsage();
            return ClientConstants.EXIT_CODE_PARSING_ERROR;
        }
        try {
            clientArguments = ClientArguments.parseArgs(args);
        } catch (ParseException exp) {
            exp.printStackTrace();
            return ClientConstants.EXIT_CODE_PARSING_ERROR;
        }
        if ( clientArguments.getPrintHelp()) {
            ClientArguments.printUsage();
            return ClientConstants.EXIT_CODE_PRINT_HELP;
        }

        // set log level according to verbose level
        Logger.getRootLogger().removeAllAppenders();
        ConsoleAppender console = new ConsoleAppender();
        if (clientArguments.getVerbose()) {
            String PATTERN = "[Client] %d %p %m%n";
            console.setLayout(new PatternLayout(PATTERN));
            console.setThreshold(Level.INFO);
            LogManager.getRootLogger().setLevel(Level.INFO);
        }
        else {
            String PATTERN = "[Client] %m%n";
            console.setLayout(new PatternLayout(PATTERN));
            console.setThreshold(Level.WARN);
            LogManager.getRootLogger().setLevel(Level.INFO);
        }
        console.setTarget("System.out");
        console.activateOptions();
        Logger.getRootLogger().addAppender(console);

        // Read configuration file
        String configJson;
        try {
            configJson = IOUtils.toString(
                    new FileInputStream(clientArguments.getConfigFile()),
                    "UTF-8");
        } catch (IOException exp) {
            LOG.error("error reading config file");
            exp.printStackTrace();
            return ClientConstants.EXIT_CODE_IO_ERROR;
        }

        try {
            fs = FileSystem.get(conf);
        } catch (IOException exp) {
            exp.printStackTrace();
            return ClientConstants.EXIT_CODE_IO_ERROR;
        }

        // parse configuration file
        try {
            aMConf = AMConf.parseArgsAndJson(args, configJson);
        } catch (ParseException exp) {
            LOG.error("error parsing config file");
            exp.printStackTrace();
            return ClientConstants.EXIT_CODE_PARSING_ERROR;
        }
        return validateAMConf();
    }

    /**
     * Submit the application, if in synchronous mode, wait for application to complete and
     * output stdout of AM
     *
     * @throws IOException
     * @throws YarnException
     */
    public int run() throws IOException, YarnException {
        LOG.debug("Client started");
        yarnClient.start();

        // Get a new application id
        YarnClientApplication app = yarnClient.createApplication();
        ApplicationSubmissionContext appContext = app
                .getApplicationSubmissionContext();

        // print JOB_ID_LIST.txt
        ApplicationId appId = appContext.getApplicationId();
        PrintWriter jobIdListFile = new PrintWriter("JOB_ID_LIST.txt");
        jobIdListFile.println(appId.toString());
        jobIdListFile.close();

        // set app name, queue and type
        appContext.setKeepContainersAcrossApplicationAttempts(keepContainers);
        appContext.setQueue(clientArguments.getAmQueue());
        appContext.setApplicationType("PopCorn");
        appContext.setApplicationName(clientArguments.getAppName());

        // Set up the container launch context for the application master
        ContainerLaunchContext amContainer = Records.newRecord(ContainerLaunchContext.class);
        // Upload local files necessary for containers to hdfs and add them to localResources map
        hdfsAppDir =
                fs.getHomeDirectory().toUri().getRawPath()
                        + "/" + clientArguments.getAppName()
                        + "/" + appId.getId();
        Map<String, ContainerConf> containerConfs = aMConf.getContainerConfs();
        for (Map.Entry<String, ContainerConf> entry : containerConfs.entrySet()) {
            String containerType = entry.getKey();
            ContainerConf containerConf = entry.getValue();
            List<String> resourcePaths = containerConf.getResourcePaths();
            for (int idx = 0; idx != resourcePaths.size(); ++idx) {
                String resourcePath = resourcePaths.get(idx);
                String hdfsPath = uploadFilesIfLocal(resourcePath, hdfsAppDir + "/container/" + containerType);
                if (hdfsPath != null) {
                    resourcePaths.set(idx, hdfsPath);
                }
            }
        }

        // Upload am jar and modified configuration file to hdfs, it is necessary for application master to start.

        String amJar = aMConf.getJarPath();
        {
            String amJarHdfsPath = uploadFilesIfLocal(amJar, hdfsAppDir + "/am");
            if (amJarHdfsPath != null) {
                amJar = amJarHdfsPath;
            }
        }
        aMConf.setJarPath(amJar);

        Map<String, LocalResource> localResources = prepareLocalResources();
        // Set local resource info into app master container launch context
        amContainer.setLocalResources(localResources);

        Integer totalNumPorts = aMConf.getNumPorts();

        if (clientArguments.getIsSync() && clientArguments.getSyncPort() == null) {
            ++totalNumPorts;
        }

        boolean needObserve = false;
        for (ContainerConf containerConf : aMConf.getContainerConfs().values()) {
            if (containerConf.getObserveOutput()) {
                needObserve = true;
                break;
            }
        }

        if (needObserve) {
            ++totalNumPorts;
        }

        portArgs = PortManager.getInstance().requestPorts(totalNumPorts);

        if (clientArguments.getIsSync()) {
            try {
                syncHost = InetAddress.getLocalHost().getHostName();
            } catch (IOException exp) {
                throw new RuntimeException("Fail to get hostname, due to exception " + exp.getMessage());
            }
            if (clientArguments.getSyncPort() == null) {
                syncPort = portArgs.get(0);
                portArgs.remove(0);
            } else {
                syncPort = clientArguments.getSyncPort();
            }
            threadPool.execute(new SyncRunnable(syncPort));
        }

        if (needObserve) {
            receiverPort = clientArguments.getReceiverPort() == null ? portArgs.get(0) : clientArguments.getReceiverPort();
            portArgs.remove(0);
        }

        // Set the env variables to be setup in the env where the application
        // master will be run
        LOG.info("Set the environment for the application master");

        Map<String, String> env = prepareEnvironment();
        amContainer.setEnvironment(env);

        List<String> commands = prepareCommand();
        amContainer.setCommands(commands);

        // Set up resource type requirements
        // For now, both memory and vcores are supported, so we set memory and
        // vcores requirements
        Resource capability = Records.newRecord(Resource.class);
        capability.setMemory(clientArguments.getAmMemory());
        capability.setVirtualCores(clientArguments.getAmVCores());
        appContext.setResource(capability);


        // Setup security tokens
        if (UserGroupInformation.isSecurityEnabled()) {
            Credentials credentials = new Credentials();
            String tokenRenewer = conf.get(YarnConfiguration.RM_PRINCIPAL);
            if (tokenRenewer == null || tokenRenewer.length() == 0) {
                throw new IOException(
                        "Can't get Master Kerberos principal for the RM to use as renewer");
            }

            // For now, only getting tokens for the default file-system.
            final Token<?> tokens[] = fs.addDelegationTokens(tokenRenewer, credentials);
            if (tokens != null) {
                for (Token<?> token : tokens) {
                    LOG.info("Got dt for " + fs.getUri() + "; " + token);
                }
            }
            DataOutputBuffer dob = new DataOutputBuffer();
            credentials.writeTokenStorageToStream(dob);
            ByteBuffer fsTokens = ByteBuffer.wrap(dob.getData(), 0, dob.getLength());
            amContainer.setTokens(fsTokens);
        }

        appContext.setAMContainerSpec(amContainer);

        // Set the priority for the application master
        Priority pri = Priority.newInstance(clientArguments.getAmPriority());
        appContext.setPriority(pri);

        // Set the queue to which this application is to be submitted in the RM
        appContext.setQueue(clientArguments.getAmQueue());

        // Submit the application to the applications manager
        // SubmitApplicationResponse submitResp =
        // applicationsManager.submitApplication(appRequest);
        // Ignore the response as either a valid response object is returned on
        // success
        // or an exception thrown to denote some form of a failure
        LOG.info("Submitting application to ASM");

        yarnClient.submitApplication(appContext);

        // Monitor the application
        LOG.info("Submit application successfully.\n" + "id = "
                + appId.toString());

        if (clientArguments.getIsSync()) {
            while (true) {
                // Check app status every 1 second.
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    LOG.debug("Thread sleep in monitoring loop interrupted");
                }

                // Get application report for the appId we are interested in
                ApplicationReport report = yarnClient.getApplicationReport(appId);

                YarnApplicationState state = report.getYarnApplicationState();
                FinalApplicationStatus dsStatus = report.getFinalApplicationStatus();
                if (YarnApplicationState.FINISHED.equals(state)
                        || YarnApplicationState.KILLED.equals(state)
                        || YarnApplicationState.FAILED.equals(state)) {
                    if (FinalApplicationStatus.SUCCEEDED.equals(dsStatus)) {
                        LOG.info("Application has completed successfully. Breaking monitoring loop");
                        break;
                    } else {
                        LOG.info("Application finished unsuccessfully."
                                + "\nYarnState: " + state.toString()
                                + "\nDSFinalStatus: " + dsStatus.toString()
                                + "\nDiagnostics:\n" + report.getDiagnostics()
                                + "\nBreaking monitoring loop");
                        break;
                    }
                }
            }
            threadPool.shutdown();
        }
        return ClientConstants.EXIT_CODE_SUCCESS;
    }

    private Map<String, String> prepareEnvironment() {
        Map<String, String> env = new HashMap<String, String>();

        if (clientArguments.getIsSync()) {
            env.put(AMConstants.ENV_SYNC_HOST, syncHost);
            env.put(AMConstants.ENV_SYNC_PORT, syncPort.toString());
        }
        if (receiverPort != null) {
            env.put(AMConstants.ENV_RECEIVER_PORT, receiverPort.toString());
        }
        env.put(AMConstants.ENV_HDFS_APP_DIR, hdfsAppDir);
        env.put(AMConstants.ENV_ALLOCTIME, clientArguments.getAllocationTimeout().toString());
        env.put(AMConstants.ENV_EXECTIME, clientArguments.getExecutionTimeout().toString());
        for (int i = 0; i != aMConf.getNumPorts(); ++i) {
            env.put(AMConstants.ENV_PORT_ARG + "_" + i, portArgs.get(i).toString());
        }
        // Add deployer.jar location to classpath
        // At some point we should not be required to add
        // the hadoop specific classpaths to the env.
        // It should be provided out of the box.
        // For now setting all required classpaths including
        // the classpath to "." for the application jar
        StringBuilder classPathEnv = new StringBuilder(Environment.CLASSPATH.$$())
                .append(ApplicationConstants.CLASS_PATH_SEPARATOR).append("./*");
        for (String c : conf.getStrings(
                YarnConfiguration.YARN_APPLICATION_CLASSPATH,
                YarnConfiguration.DEFAULT_YARN_CROSS_PLATFORM_APPLICATION_CLASSPATH)) {
            classPathEnv.append(ApplicationConstants.CLASS_PATH_SEPARATOR);
            classPathEnv.append(c.trim());
        }
        classPathEnv.append(ApplicationConstants.CLASS_PATH_SEPARATOR).append(
                "./log4j.properties");

        // add the runtime classpath needed for tests to work
        if (conf.getBoolean(YarnConfiguration.IS_MINI_YARN_CLUSTER, false)) {
            classPathEnv.append(':');
            classPathEnv.append(System.getProperty("java.class.path"));
        }

        env.put("CLASSPATH", classPathEnv.toString());

        return env;
    }

    private Map<String, LocalResource> prepareLocalResources() throws IOException {
        // Set local resources for the application master
        // Local files or archives as needed
        // In this scenario, the jar file for the application master is part of the local resources
        Map<String, LocalResource> localResources = new HashMap<String, LocalResource>();

        LocalResourceUtils.addHdfsFiles(fs, new Path(aMConf.getJarPath()), "", localResources);

        String jsonConfig = new Gson().toJson(aMConf);
        Path configPath = new Path(hdfsAppDir + "/am/" + AMConstants.CONFIG_FILE);
        OutputStream os = fs.create(configPath);
        IOUtils.write(jsonConfig, os);
        os.close();
        LocalResourceUtils.addHdfsFiles(fs, configPath, "", localResources);

        return localResources;
    }

    private List<String> prepareCommand() {
        // Set the necessary command to execute the application master
        Vector<CharSequence> vargs = new Vector<CharSequence>(30);

        // Set java executable command
        LOG.info("Setting up app master command");

        vargs.add(Environment.JAVA_HOME.$$() + "/bin/java");
        vargs.add(" -Xmx" + clientArguments.getAmMemory() + "m");
        // Set class name
        vargs.add(amClassName);
        vargs.add("1>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR
                + "/" + "AppMaster.stdout");
        vargs.add("2>" + ApplicationConstants.LOG_DIR_EXPANSION_VAR
                + "/" + "AppMaster.stderr");

        // Get final commmand
        StringBuilder command = new StringBuilder();
        for (CharSequence str : vargs) {
            command.append(str).append(" ");
        }

        LOG.info("Completed setting up app master command " + command.toString());
        List<String> commands = new ArrayList<String>();
        commands.add(command.toString());
        return commands;
    }

    private int validateAMConf() {
        boolean result = true;
        if (aMConf.getJarPath() == null) {
            LOG.error("jarPath is not set");
            result = false;
        }
        Map<String, ContainerConf> containerConfs = aMConf.getContainerConfs();
        // Check correctness of exclusions
        // Every element in exclusions should be an array with 2-size. Container type in the arrays should be contained
        // containerConfs
        List<List<String>> exclusions = aMConf.getExclusions();
        for (List<String> exclusion : exclusions) {
            if (exclusion.size() != 2) {
                LOG.error("Inner array can have only 2 elements");
                result = false;
            } else {
                for (String containerType : exclusion) {
                    if (!containerConfs.containsKey(containerType)) {
                        LOG.error(String.format(
                                "Container type %s in exclusions doesn't exist in containerConfs",
                                containerType
                        ));
                        result = false;
                    }
                }
            }
        }
        // Check correctness of containerConfs
        List<String> existedGenEnvVars = new ArrayList<String>(Arrays.asList(
                AMConstants.GEN_ENV_HOST, AMConstants.GEN_ENV_IP,
                AMConstants.GEN_ENV_RANK, AMConstants.GEN_ENV_NUM_PROCESSES
        ));
        for (int i = 0; i != aMConf.getNumPorts(); ++i) {
            existedGenEnvVars.add(AMConstants.ENV_PORT_ARG + "_" + i);
        }
        for (Map.Entry<String, ContainerConf> entry : containerConfs.entrySet()) {
            String containerType = entry.getKey();
            ContainerConf containerConf = entry.getValue();
            // Make sure attributes without default value are not null
            List<String> notNullAttributes = Arrays.asList(
                    "numProcesses", "processMemory", "processVCores", "executeDir", "executeFilename"
            );
            for (String attribute : notNullAttributes) {
                try {
                    Method method = containerConf.getClass().getMethod(
                            "get" + Character.toUpperCase(attribute.charAt(0)) + attribute.substring(1));
                    if (method.invoke(containerConf) == null) {
                        LOG.error(String.format(
                                "%s of container type %s has not been set",
                                attribute,
                                containerType
                        ));
                        result = false;
                    }
                } catch (Exception exp) {
                }
            }
            // Check whether the resource paths existed.
            List<String> resourcePaths = containerConf.getResourcePaths();
            for (String resourcePath : resourcePaths) {
                if (!fileExisted(resourcePath)) {
                    LOG.error(String.format(
                            "resourcePath %s of container type %s does not existed.",
                            resourcePath,
                            containerType));
                    result = false;
                }
            }
            // Check whether execute file exists.
            boolean hasExecuteFile = false;
            for (String resourcePath : resourcePaths) {
                String executeFilePath;
                boolean isHdfsPath = resourcePath.toLowerCase().startsWith("hdfs://");
                if (isHdfsPath) {
                    resourcePath = resourcePath.substring(7);
                }
                java.nio.file.Path prevPath = Paths.get(resourcePath).getParent();
                java.nio.file.Path nextPath = Paths.get(containerConf.getExecuteDir(),
                        containerConf.getExecuteFilename());
                executeFilePath = Paths.get(
                        prevPath == null ? "" : prevPath.toString(),
                        nextPath.toString()).toString();
                if (isHdfsPath) {
                    executeFilePath = "hdfs://" + executeFilePath.replace("\\", "/");
                }
                if (fileExisted(executeFilePath)) {
                    hasExecuteFile = true;
                }
            }
            if (!hasExecuteFile) {
                LOG.error(String.format(
                        "Execute file of container type %s does not existed.",
                        containerType));
                result = false;
            }
            // If containerConf is set to flexible, it cannot be waited for completion
            if (containerConf.getFlexible() && containerConf.getWaitForCompletion()) {
                LOG.error(String.format(
                        "container type %s is flexible, so it cannot be waited for completion.",
                        containerType));
                result = false;
            }
        }
        // Check correctness of launchOrder
        // For launchOrder, whether container type in launchOrder exists.
        List<String> launchOrder = aMConf.getLaunchOrder();
        for (String containerType : launchOrder) {
            if (!containerConfs.containsKey(containerType)) {
                LOG.error(String.format(
                        "Container type %s in launchOrder doesn't exist in containerConfs",
                        containerType
                ));
                result = false;
            }
        }
        Set<String> containerTypeSet = new HashSet<String>(launchOrder);
        if (containerTypeSet.size() != launchOrder.size()) {
            LOG.error("launchOrder contains same container type");
            result = false;
        }
        if (!result) {
            LOG.error("Failed to initialized Client, due to errors in command line or configuration file");
        }

        return result ? 0 : 1;
    }

    private class SyncRunnable implements Runnable {

        private int syncPort;

        public SyncRunnable(int syncPort) {
            this.syncPort = syncPort;
        }

        @Override
        public void run() {
            try {
                ServerSocket server = new ServerSocket(syncPort);
                while (true) {
                    Socket socket = server.accept();
                    LOG.info("Receive sync socket from " + socket.getLocalAddress().getHostName());
                    threadPool.execute(new SyncHandlerRunnable(socket));
                }
            } catch (IOException exp) {
                LOG.error("Fail to sync application master due to excpetion " + exp.getMessage());
            }
        }
    }

    private class SyncHandlerRunnable implements Runnable {

        private Socket socket;

        private SyncHandlerRunnable(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                LoggingEvent event;
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                while ((event = (LoggingEvent) ois.readObject()) != null) {
                    System.out.println(String.format(
                            "[AM] %s,%03d %s %s",
                            formatter.format(new Date(event.getTimeStamp())),
                            event.getTimeStamp() % 1000,
                            event.getLevel(),
                            event.getMessage()));
                }
                ois.close();
                socket.close();
            } catch (Exception exp) {
                LOG.error("Socket fail due to exception " + exp.getMessage());
            }
        }
    }

    private boolean fileExisted(String resourcePath) {
        if (resourcePath.startsWith("hdfs://")) {
            // If resource path is hdfs path, we will check if the file is existed
            Path path = new Path(resourcePath);
            try {
                FileStatus fileStatus = fs.getFileStatus(path);
            } catch (IOException exp) {
                return false;
            }
            return true;
        } else {
            // If resource path is local path, we will check if the local file is existed,
            // upload the local local file and replace local path to hdfs path.
            File file = new File(resourcePath);
            return file.exists();
        }
    }

    private String uploadFilesIfLocal(String resourcePath, String hdfsDir) throws IOException {
        if (resourcePath.startsWith("hdfs://")) {
            // If resource path is hdfs path, we will check if the file is existed
            Path path = new Path(resourcePath);
            try {
                FileStatus fileStatus = fs.getFileStatus(path);
            } catch (IOException exp) {
                throw new RuntimeException(
                        String.format("hdfs resource path \"%s\" failed to access", resourcePath));
            }
            return null;
        } else {
            // If resource path is local path, we will check if the local file is existed,
            // upload the local local file and replace local path to hdfs path.
            File file = new File(resourcePath);
            if (!file.exists()) {
                throw new RuntimeException(
                        String.format("local resource path \"%s\" is not existed", resourcePath));
            }
            String hdfsPath = hdfsDir + "/" + file.getName();
            Path srcPath = new Path(resourcePath);
            Path dstPath = new Path(hdfsPath);
            fs.copyFromLocalFile(srcPath, dstPath);
            return hdfsPath;
        }
    }

}
