package org.apache.hadoop.yarn.applications.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.yarn.applications.am.ContainerPlacementStrategy;
import org.apache.hadoop.yarn.applications.constants.AMConstants;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Map;
import java.util.Set;

/**
 * Created by v-yihhe on 1/19/2016.
 */
public class MachineListFormatter {

  public static String format(
      String machineListFormat,
      Map<String, String> genEnvToValue,
      Map<String, String> envVars) {
    String result = machineListFormat;
    for (Map.Entry<String, String> entry : genEnvToValue.entrySet()) {
      result = result.replaceAll("%" + entry.getKey() + "%", entry.getValue());
    }
    for (Map.Entry<String, String> entry : envVars.entrySet()) {
      result = result.replaceAll("%" + entry.getKey() + "%", entry.getValue());
    }
    return result;
  }
}
